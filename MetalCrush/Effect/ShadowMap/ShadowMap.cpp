#include "ShadowMap.h"
#include "WinApp.h"
#include "Input.h"
#include "DirectXCommon.h"

#include <cassert>
#include <d3dx12.h>
#include <d3dcompiler.h>

#pragma comment(lib, "d3dcompiler.lib")

using namespace DirectX;
using namespace Microsoft::WRL;

ShadowMap::ShadowMap()
{

}

ShadowMap* ShadowMap::GetInstance()
{
	static ShadowMap shadowMap;

	return &shadowMap;
}

void ShadowMap::CreateGraphicsPipelineState()
{
	HRESULT result;

	ComPtr<ID3DBlob> vsBlob; // 頂点シェーダオブジェクト
	ComPtr<ID3DBlob> psBlob;	// ピクセルシェーダオブジェクト
	ComPtr<ID3DBlob> errorBlob; // エラーオブジェクト

	// 頂点シェーダの読み込みとコンパイル
	result = D3DCompileFromFile(
		L"Resources/Shaders/ShadowMap/ShadowMapVS.hlsl",	// シェーダファイル名
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // インクルード可能にする
		"main", "vs_5_0",	// エントリーポイント名、シェーダーモデル指定
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, // デバッグ用設定
		0,

		&vsBlob, &errorBlob);
	if (FAILED(result)) {
		// errorBlobからエラー内容をstring型にコピー
		std::string errstr;
		errstr.resize(errorBlob->GetBufferSize());

		std::copy_n((char*)errorBlob->GetBufferPointer(),
			errorBlob->GetBufferSize(),
			errstr.begin());
		errstr += "\n";
		// エラー内容を出力ウィンドウに表示
		OutputDebugStringA(errstr.c_str());
		assert(0);
	}

	// ピクセルシェーダの読み込みとコンパイル
	result = D3DCompileFromFile(
		L"Resources/Shaders/ShadowMap/ShadowMapPS.hlsl",	// シェーダファイル名
		nullptr,
		D3D_COMPILE_STANDARD_FILE_INCLUDE, // インクルード可能にする
		"main", "ps_5_0",	// エントリーポイント名、シェーダーモデル指定
		D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION, // デバッグ用設定
		0,
		&psBlob, &errorBlob);
	if (FAILED(result)) {
		// errorBlobからエラー内容をstring型にコピー
		std::string errstr;
		errstr.resize(errorBlob->GetBufferSize());

		std::copy_n((char*)errorBlob->GetBufferPointer(),
			errorBlob->GetBufferSize(),
			errstr.begin());
		errstr += "\n";
		// エラー内容を出力ウィンドウに表示
		OutputDebugStringA(errstr.c_str());
		assert(0);
	}

	// 頂点レイアウト
	D3D12_INPUT_ELEMENT_DESC inputLayout[] = {
		{ // xy座標(1行で書いたほうが見やすい)
			"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,
			D3D12_APPEND_ALIGNED_ELEMENT,
			D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
		},
		{ // uv座標(1行で書いたほうが見やすい)
			"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,
			D3D12_APPEND_ALIGNED_ELEMENT,
			D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0
		},
	};

	// グラフィックスパイプラインの流れを設定
	D3D12_GRAPHICS_PIPELINE_STATE_DESC gpipeline{};
	gpipeline.VS = CD3DX12_SHADER_BYTECODE(vsBlob.Get());
	gpipeline.PS = CD3DX12_SHADER_BYTECODE(psBlob.Get());

	// サンプルマスク
	gpipeline.SampleMask = D3D12_DEFAULT_SAMPLE_MASK; // 標準設定

	// ラスタライザステート
	gpipeline.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	gpipeline.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;

	// デプスステンシルステート
	gpipeline.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	gpipeline.DepthStencilState.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS; // 常に上書きルール

	// レンダーターゲットのブレンド設定
	D3D12_RENDER_TARGET_BLEND_DESC blenddesc{};
	blenddesc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;	// RBGA全てのチャンネルを描画
	blenddesc.BlendEnable = true;
	blenddesc.BlendOp = D3D12_BLEND_OP_ADD;
	blenddesc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blenddesc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;

	blenddesc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blenddesc.SrcBlendAlpha = D3D12_BLEND_ONE;
	blenddesc.DestBlendAlpha = D3D12_BLEND_ZERO;

	// ブレンドステートの設定
	gpipeline.BlendState.RenderTarget[0] = blenddesc;

	// 深度バッファのフォーマット
	gpipeline.DSVFormat = DXGI_FORMAT_D32_FLOAT;

	// 頂点レイアウトの設定
	gpipeline.InputLayout.pInputElementDescs = inputLayout;
	gpipeline.InputLayout.NumElements = _countof(inputLayout);

	// 図形の形状設定（三角形）
	gpipeline.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	gpipeline.NumRenderTargets = 1;	// 描画対象は1つ
	gpipeline.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM; // 0〜255指定のRGBA
	gpipeline.SampleDesc.Count = 1; // 1ピクセルにつき1回サンプリング

	// デスクリプタレンジ
	CD3DX12_DESCRIPTOR_RANGE descRangeSRV[2] = {};
	descRangeSRV[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0);
	descRangeSRV[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 1);

	// ルートパラメータ
	CD3DX12_ROOT_PARAMETER rootparams[3] = {};
	rootparams[0].InitAsConstantBufferView(0, 0, D3D12_SHADER_VISIBILITY_ALL);
	rootparams[1].InitAsDescriptorTable(1, &descRangeSRV[0], D3D12_SHADER_VISIBILITY_ALL);
	rootparams[2].InitAsDescriptorTable(1, &descRangeSRV[1], D3D12_SHADER_VISIBILITY_ALL);

	// スタティックサンプラー
	CD3DX12_STATIC_SAMPLER_DESC samplerDesc = CD3DX12_STATIC_SAMPLER_DESC(0, D3D12_FILTER_MIN_MAG_MIP_POINT);

	// ルートシグネチャの設定
	CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC rootSignatureDesc;
	rootSignatureDesc.Init_1_0(_countof(rootparams), rootparams, 1, &samplerDesc, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ComPtr<ID3DBlob> rootSigBlob;
	// バージョン自動判定のシリアライズ
	result = D3DX12SerializeVersionedRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1_0, &rootSigBlob, &errorBlob);
	assert(SUCCEEDED(result));

	// ルートシグネチャの生成
	result = m_dev->CreateRootSignature(0, rootSigBlob->GetBufferPointer(), rootSigBlob->GetBufferSize(), IID_PPV_ARGS(&m_rootsignature));
	assert(SUCCEEDED(result));

	gpipeline.pRootSignature = m_rootsignature.Get();

	// グラフィックスパイプラインの生成
	result = m_dev->CreateGraphicsPipelineState(&gpipeline, IID_PPV_ARGS(&m_pipelinestate));
	assert(SUCCEEDED(result));
}

void ShadowMap::CreateVertexBuffer()
{
	HRESULT result;

	// 頂点バッファの生成
	result = m_dev->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(VertexPosUv) * 4),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&m_vertBuff));
	assert(SUCCEEDED(result));

	// 頂点データ
	VertexPosUv vertices[4] = {
		{ { -1.0f, -1.0f, 0.0f }, { 0.0f, 1.0f } },
		{ { -1.0f, +1.0f, 0.0f }, { 0.0f, 0.0f } },
		{ { +1.0f, -1.0f, 0.0f }, { 1.0f, 1.0f } },
		{ { +1.0f, +1.0f, 0.0f }, { 1.0f, 0.0f } },
	};

	// 頂点バッファへのデータ転送
	VertexPosUv* vertMap = nullptr;
	result = m_vertBuff->Map(0, nullptr, (void**)&vertMap);
	memcpy(vertMap, vertices, sizeof(vertices));
	m_vertBuff->Unmap(0, nullptr);

	// 頂点バッファビューの作成
	m_vbView.BufferLocation = m_vertBuff->GetGPUVirtualAddress();
	m_vbView.SizeInBytes = sizeof(VertexPosUv) * 4;
	m_vbView.StrideInBytes = sizeof(VertexPosUv);
}

void ShadowMap::CreateConstBuffer()
{
	HRESULT result;

	result = m_dev->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer((sizeof(ConstBufferData) + 0xff) & ~0xff),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&m_constBuff)
	);
	assert(SUCCEEDED(result));
}

void ShadowMap::CreateTexture()
{
	HRESULT result;

	// リソース設定
	CD3DX12_RESOURCE_DESC texresDesc = CD3DX12_RESOURCE_DESC::Tex2D(
		DXGI_FORMAT_R8G8B8A8_UNORM,
		WinApp::window_width,
		WinApp::window_height,
		1, 0, 1, 0, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET
	);

	// テクスチャバッファの生成
	float R = 86;
	float G = 119;
	float B = 252;
	float A = 1.0f;
	float clearColor[4] = { powf((R / 255), 2.2f), powf((G / 255), 2.2f), powf((B / 255), 2.2f), A };
	result = m_dev->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_CPU_PAGE_PROPERTY_WRITE_BACK, D3D12_MEMORY_POOL_L0),
		D3D12_HEAP_FLAG_NONE,
		&texresDesc,
		D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
		&CD3DX12_CLEAR_VALUE(DXGI_FORMAT_R8G8B8A8_UNORM, clearColor),
		IID_PPV_ARGS(&m_texBuff)
	);
	assert(SUCCEEDED(result));

	// テクスチャを赤クリア
	// 画素数
	const UINT pixelCount = WinApp::window_width * WinApp::window_height;
	// 画素数一行分のデータサイズ
	const UINT rowPitch = sizeof(UINT) * WinApp::window_width;
	// 画像全体のデータサイズ
	const UINT depthPitch = rowPitch * WinApp::window_height;
	// 画像イメージ
	UINT* img = new UINT[pixelCount];
	for (int i = 0; i < pixelCount; i++)
	{
		img[i] = 0xff0000ff;
	}

	// テクスチャバッファにデータ転送
	result = m_texBuff->WriteToSubresource(0, nullptr, img, rowPitch, depthPitch);
	assert(SUCCEEDED(result));
	delete[] img;
}

void ShadowMap::CreateShaderResources()
{
	HRESULT result;

	// テクスチャの作成
	CreateTexture();

	// SRV用デスクリプタヒープを設定
	D3D12_DESCRIPTOR_HEAP_DESC srvDescHeapDesc{};
	srvDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	srvDescHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	srvDescHeapDesc.NumDescriptors = 2;

	// SRV用デスクリプタヒープを生成
	result = m_dev->CreateDescriptorHeap(&srvDescHeapDesc, IID_PPV_ARGS(&m_descHeapSRV));
	assert(SUCCEEDED(result));

	// シェーダリソースビュー設定
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MipLevels = 1;

	// デスクリプタヒープにシェーダリソースビュー作成
	m_dev->CreateShaderResourceView(
		m_texBuff.Get(),
		&srvDesc,
		CD3DX12_CPU_DESCRIPTOR_HANDLE(
			m_descHeapSRV->GetCPUDescriptorHandleForHeapStart(),
			0,
			m_dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV))
	);

	// デスクリプタヒープに深度値テクスチャ用シェーダリソースビュー作成
	srvDesc.Format = DXGI_FORMAT_R32_FLOAT;
	m_dev->CreateShaderResourceView
	(
		DirectXCommon::GetInstance()->GetDepthBuffer(),
		&srvDesc,
		CD3DX12_CPU_DESCRIPTOR_HANDLE(
			m_descHeapSRV->GetCPUDescriptorHandleForHeapStart(),
			1,
			m_dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV))
	);
}

void ShadowMap::CreateRenderTarget()
{
	HRESULT result;

	// RTV用デスクリプタヒープを設定
	D3D12_DESCRIPTOR_HEAP_DESC rtvDescHeapDesc{};
	rtvDescHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	rtvDescHeapDesc.NumDescriptors = 1;

	// RTV用デスクリプタヒープを生成
	result = m_dev->CreateDescriptorHeap(&rtvDescHeapDesc, IID_PPV_ARGS(&m_descHeapRTV));
	assert(SUCCEEDED(result));

	// デスクリプタヒープにレンダーターゲットビュー作成
	m_dev->CreateRenderTargetView(
		m_texBuff.Get(),
		nullptr,
		m_descHeapRTV->GetCPUDescriptorHandleForHeapStart()
	);
}

void ShadowMap::CreateDepthBuffer()
{
	HRESULT result;

	// 深度バッファリソース設定
	CD3DX12_RESOURCE_DESC depthResDesc = CD3DX12_RESOURCE_DESC::Tex2D(
		DXGI_FORMAT_D32_FLOAT,
		WinApp::window_width,
		WinApp::window_height,
		1, 0,
		1, 0,
		D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL
	);

	// 深度バッファの生成
	result = m_dev->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&depthResDesc,
		D3D12_RESOURCE_STATE_DEPTH_WRITE,
		&CD3DX12_CLEAR_VALUE(DXGI_FORMAT_D32_FLOAT, 1.0f, 0),
		IID_PPV_ARGS(&m_depthBuff)
	);
	assert(SUCCEEDED(result));

	// DSV用デスクリプタヒープを設定
	D3D12_DESCRIPTOR_HEAP_DESC descHeapDesc{};
	descHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	descHeapDesc.NumDescriptors = 1;

	// DSV用デスクリプタヒープを生成
	result = m_dev->CreateDescriptorHeap(&descHeapDesc, IID_PPV_ARGS(&m_descHeapDSV));
	assert(SUCCEEDED(result));

	// デプスステンシルビュー設定
	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc = {};
	dsvDesc.Format = DXGI_FORMAT_D32_FLOAT;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;

	// デスクリプタヒープにデプスステンシルビュー作成
	m_dev->CreateDepthStencilView(
		m_depthBuff.Get(),
		&dsvDesc,
		m_descHeapDSV->GetCPUDescriptorHandleForHeapStart()
	);
}

void ShadowMap::Initialize(ID3D12Device* dev)
{
	// nullチェック
	if (dev == nullptr)
	{
		assert(0);
	}
	m_dev = dev;

	// パイプライン作成
	CreateGraphicsPipelineState();

	// 頂点バッファ作成
	CreateVertexBuffer();

	// 定数バッファ作成
	CreateConstBuffer();

	// シェーダーリソース作成
	CreateShaderResources();

	// レンダーターゲット作成
	CreateRenderTarget();

	// 深度バッファ作成
	CreateDepthBuffer();
}

void ShadowMap::Draw(ID3D12GraphicsCommandList* cmdList)
{
	if (cmdList == nullptr)
	{
		assert(0);
	}

	// 定数バッファにデータ転送
	ConstBufferData* constMap = nullptr;
	HRESULT result = m_constBuff->Map(0, nullptr, (void**)&constMap);
	if (SUCCEEDED(result))
	{
		constMap->mat = XMMatrixIdentity();
		m_constBuff->Unmap(0, nullptr);
	}

	// パイプラインとルートシグネチャの設定
	cmdList->SetPipelineState(m_pipelinestate.Get());
	cmdList->SetGraphicsRootSignature(m_rootsignature.Get());

	// プリミティブ形状を設定
	cmdList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

	// 頂点バッファをセット
	cmdList->IASetVertexBuffers(0, 1, &m_vbView);

	// デスクリプタヒープをセット
	ID3D12DescriptorHeap* ppHeaps[] = { m_descHeapSRV.Get() };
	cmdList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	// 定数バッファをセット
	cmdList->SetGraphicsRootConstantBufferView(0, m_constBuff->GetGPUVirtualAddress());

	// シェーダリソースビューをセット
	for (int i = 0; i < 2; i++)
	{
		cmdList->SetGraphicsRootDescriptorTable(1 + i,
			CD3DX12_GPU_DESCRIPTOR_HANDLE(
				m_descHeapSRV->GetGPUDescriptorHandleForHeapStart(),
				i,
				m_dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV)
			)
		);
	}

	// ポリゴンの描画（４頂点で四角形）
	cmdList->DrawInstanced(4, 1, 0, 0);
}

void ShadowMap::PreDrawScene(ID3D12GraphicsCommandList* cmdList)
{
	//リソースバリア変更
	cmdList->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(
			m_texBuff.Get(),
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE,
			D3D12_RESOURCE_STATE_RENDER_TARGET)
	);

	//RTV用デスクリプタヒープのハンドルを取得
	D3D12_CPU_DESCRIPTOR_HANDLE rtvHs = m_descHeapRTV->GetCPUDescriptorHandleForHeapStart();
	//DSV用デスクリプタヒープのハンドルを取得
	D3D12_CPU_DESCRIPTOR_HANDLE dsvH = m_descHeapDSV->GetCPUDescriptorHandleForHeapStart();
	//レンダーターゲットビューをセット
	cmdList->OMSetRenderTargets(1, &rtvHs, false, &dsvH);

	CD3DX12_VIEWPORT viewports = CD3DX12_VIEWPORT(0.0f, 0.0f, WinApp::window_width, WinApp::window_height);
	CD3DX12_RECT scissorRects = CD3DX12_RECT(0, 0, WinApp::window_width, WinApp::window_height);

	//ビューポートの設定
	cmdList->RSSetViewports(1, &viewports);
	//シザリング矩形の設定
	cmdList->RSSetScissorRects(1, &scissorRects);

	//全画面クリア
	float R = 86;
	float G = 119;
	float B = 252;
	float A = 1.0f;
	float clearColor[4] = { powf((R / 255), 2.2f), powf((G / 255), 2.2f), powf((B / 255), 2.2f), A };
	cmdList->ClearRenderTargetView(rtvHs, clearColor, 0, nullptr);

	//深度バッファクリア
	cmdList->ClearDepthStencilView(dsvH, D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr);
}

void ShadowMap::PostDrawScene(ID3D12GraphicsCommandList* cmdList)
{
	//リソースバリア変更
	cmdList->ResourceBarrier(1,
		&CD3DX12_RESOURCE_BARRIER::Transition(
			m_texBuff.Get(),
			D3D12_RESOURCE_STATE_RENDER_TARGET,
			D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE)
	);
}
