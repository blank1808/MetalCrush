#pragma once
#include <Windows.h>
#include <wrl.h>
#include <d3d12.h>
#include <DirectXMath.h>

class ShadowMap
{
public: // エイリアス
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // サブクラス
	// 頂点データ構造体
	struct VertexPosUv
	{
		XMFLOAT3 pos; // 座標
		XMFLOAT2 uv;  // uv
	};

	// 定数バッファ構造体
	struct ConstBufferData
	{
		XMMATRIX mat; // 変換行列
	};

private: //メンバ変数
	//	デバイス
	ID3D12Device* m_dev = nullptr;
	// 頂点バッファ
	ComPtr<ID3D12Resource> m_vertBuff;
	// 頂点バッファビュー
	D3D12_VERTEX_BUFFER_VIEW m_vbView = {};
	// 定数バッファ
	ComPtr<ID3D12Resource> m_constBuff;
	// テクスチャリソース（テクスチャバッファ）
	ComPtr<ID3D12Resource> m_texBuff;
	// 深度バッファ
	ComPtr<ID3D12Resource> m_depthBuff;
	// テクスチャ用デスクリプタヒープの生成
	ComPtr<ID3D12DescriptorHeap> m_descHeapSRV;
	// レンダーターゲットビュー用デスクリプタヒープの生成
	ComPtr<ID3D12DescriptorHeap> m_descHeapRTV;
	// デプスステンシルビュー用デスクリプタヒープの生成
	ComPtr<ID3D12DescriptorHeap> m_descHeapDSV;
	// パイプラインステートオブジェクト
	ComPtr<ID3D12PipelineState> m_pipelinestate;
	// ルートシグネチャ
	ComPtr<ID3D12RootSignature> m_rootsignature;

public: // 静的メンバ関数
	/// <summary>
	/// インスタンス取得
	/// </summary>
	static ShadowMap* GetInstance();

public: //メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	ShadowMap();

	/// <summary>
	/// 頂点バッファの作成
	/// </summary>
	void CreateVertexBuffer();

	/// <summary>
	/// 定数バッファの作成
	/// </summary>
	void CreateConstBuffer();

	/// <summary>
	/// テクスチャの作成
	/// </summary>
	void CreateTexture();

	/// <summary>
	/// シェーダーリソースの作成
	/// </summary>
	void CreateShaderResources();

	/// <summary>
	/// 深度バッファの作成
	/// </summary>
	void CreateDepthBuffer();

	/// <summary>
	/// パイプラインの作成
	/// </summary>
	void CreateGraphicsPipelineState();

	/// <summary>
	/// レンダーターゲットの作成
	/// </summary>
	void CreateRenderTarget();

	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize(ID3D12Device* dev);

	/// <summary>
	/// 描画コマンド
	/// </summary>
	/// <param name="cmdList">コマンドリスト</param>
	void Draw(ID3D12GraphicsCommandList* cmdList);

	/// <summary>
	/// シーン描画前処理
	/// </summary>
	/// <param name="cmdList">コマンドリスト</param>
	void PreDrawScene(ID3D12GraphicsCommandList* cmdList);

	/// <summary>
	/// シーン描画後処理
	/// </summary>
	/// <param name="cmdList">コマンドリスト</param>
	void PostDrawScene(ID3D12GraphicsCommandList* cmdList);
};