#include "OnoGame.h"
#include "SceneFactory.h"
#include "Sprite.h"
#include "Object3d.h"

void OnoGame::LoadResources()
{
	// スプライトテクスチャ読み込み
	Sprite::LoadTexture(0, L"Resources/DebugFont/DebugFont.png");
	Sprite::LoadTexture(1, L"Resources/Reticle.png");
	Sprite::LoadTexture(2, L"Resources/NumberText.png");
	Sprite::LoadTexture(3, L"Resources/HPFrame.png");
	Sprite::LoadTexture(4, L"Resources/HPGauge.png");
	Sprite::LoadTexture(5, L"Resources/BoosterFrame.png");
	Sprite::LoadTexture(6, L"Resources/BoosterGauge.png");
	Sprite::LoadTexture(7, L"Resources/GameOverScreen.png");
	Sprite::LoadTexture(8, L"Resources/NBulletNumFrame.png");
	Sprite::LoadTexture(9, L"Resources/RaderAnimation.png");
	Sprite::LoadTexture(10, L"Resources/RaderEnemy.png");
	Sprite::LoadTexture(11, L"Resources/startScreen.png");
	Sprite::LoadTexture(12, L"Resources/EndScreen.png");
	Sprite::LoadTexture(13, L"Resources/CameraOrder.png");
	Sprite::LoadTexture(14, L"Resources/RaderFrame.png");
	Sprite::LoadTexture(15, L"Resources/EndScreen2.png");
	Sprite::LoadTexture(16, L"Resources/Order.png");
	Sprite::LoadTexture(17, L"Resources/FreeOrder.png");
	Sprite::LoadTexture(18, L"Resources/MoveOrder.png");
	Sprite::LoadTexture(19, L"Resources/JumpOrder.png");
	Sprite::LoadTexture(20, L"Resources/DashOrder.png");
	Sprite::LoadTexture(21, L"Resources/NbulletOrder.png");
	Sprite::LoadTexture(22, L"Resources/HbulletOrder.png");
	Sprite::LoadTexture(23, L"Resources/HBulletNumFrame.png");
	Sprite::LoadTexture(100, L"Resources/Default/white1280x720.png");

	// OBJモデル読み込み
	Object3d::LoadModel(0, "Desert", true);
	Object3d::LoadModel(1, "SkyWall");
	Object3d::LoadModel(2, "Tank");
	Object3d::LoadModel(3, "Bullet", true);
	Object3d::LoadModel(4, "Leg");
	Object3d::LoadModel(5, "Body");
	Object3d::LoadModel(6, "Arm");
	Object3d::LoadModel(7, "PlaneStage");

	// FBXモデル読み込み

}

void OnoGame::Initialize()
{
	//基底クラスの初期化
	FrameWork::Initialize();

	//シーンの初期化
	scene_factory.reset(new SceneFactory);
	SceneManager::GetInstance()->SetSceneFactory(scene_factory.get());
	//シーンセット
	SceneManager::GetInstance()->ChangeScene("TITLE");

	// リソースのロード
	LoadResources();
}

void OnoGame::Finalize()
{
	//基底クラスの初期化
	FrameWork::Finalize();
}

void OnoGame::Update()
{
	//基底クラスの更新
	FrameWork::Update();
}

void OnoGame::Draw()
{
	//基底クラスの更新
	FrameWork::Draw();
}