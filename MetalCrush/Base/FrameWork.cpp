#include "FrameWork.h"

void FrameWork::Run()
{
	// ゲーム初期化
	Initialize();

	while (1)  // ゲームループ
	{
		// ゲーム毎フレーム更新
		Update();

		// ゲーム終了リクエスト
		if (end_request)
		{
			break;
		}

		// ゲーム描画
		Draw();
	}

	// ゲーム終了
	Finalize();
}

void FrameWork::Initialize()
{
	// windowsアプリケーションの初期化
	WinApp *win = WinApp::GetInstance();
	win->CreateGameWindow();

	// Input初期化;
	Input::GetInstance()->Initialize(win);

	// カメラ初期化
	Camera::GetInstance()->Initialize();

	// DirectX初期化処理
	DirectXCommon *dx_cmd = DirectXCommon::GetInstance();
	if (!dx_cmd->Initialize(win))
	{
		assert(0);
	}

	// スプライト静的初期化
	if (!Sprite::StaticInitialize(dx_cmd->GetDev()))
	{
		assert(0);
	}

	// オブジェクト静的初期化
	if (!Object3d::StaticInitialize(dx_cmd->GetDev()))
	{
		assert(0);
	}

	// パーティクル静的初期化
	if (!ParticleManager::StaticInitialize(dx_cmd->GetDev()))
	{
		assert(0);
	}

	// ライト静的初期化
	if (!LightGroup::StaticInitialize(dx_cmd->GetDev()))
	{
		assert(0);
	}

	// FBXオブジェクト静的初期化
	if (!FbxObject3d::StaticInitialize(dx_cmd->GetDev()))
	{
		assert(0);
	}

	// ライトの初期化
	LightGroup::GetInstance()->Initialize();

	// シーン切り替え初期化
	SceneChange::GetInstance()->Initialize();
}

void FrameWork::Finalize()
{
	// ウィンドウクラスを登録解除
	WinApp::GetInstance()->TerminateGameWindow();
}

void FrameWork::Update()
{
	// 終了のリクエスト
	if (WinApp::GetInstance()->ProcessMessage() || Input::GetInstance()->PushKey(DIK_ESCAPE))
	{
		end_request = true;
	}

	// キーの情報取得
	Input::GetInstance()->Update();

	// シーンの更新
	SceneManager::GetInstance()->Update();
}

void FrameWork::Draw()
{
	// 描画前処理
	DirectXCommon::GetInstance()->PreDraw();

	// シーンobject描画
	SceneManager::GetInstance()->Draw();

	// シーンSprite描画
	SceneManager::GetInstance()->DrawSprite();

	// 描画後処理
	DirectXCommon::GetInstance()->PostDraw();
}