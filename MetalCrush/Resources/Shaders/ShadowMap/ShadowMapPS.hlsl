#include "ShadowMap.hlsli"

Texture2D<float4> tex : register(t0);
Texture2D<float> depth : register(t1);
SamplerState smp : register(s0);

float4 main(VSOutput input) : SV_TARGET
{
	float result = pow(depth.Sample(smp, input.uv), 1);

	return float4(result, result, result, 1);
}
