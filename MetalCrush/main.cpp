﻿#include "OnoGame.h"
#include <SafeDelete.h>

/// <summary>
/// Windowsアプリでのエントリーポイント(main関数)
/// </summary>
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	//ポインタ置き場
	OnoGame* game = new OnoGame;

	//ゲーム開始
	game->Run();

	safe_delete(game);

	return 0;
}