#pragma once
#include "Sprite.h"

#include <DirectXMath.h>
#include <memory>

/// <summary>
/// シーン切り替え
/// </summary>
class SceneChange
{
private: // メンバ変数
	// スクリーン
	std::unique_ptr<Sprite> m_shadowScreen;
	// 色
	DirectX::XMFLOAT4 m_screenColor = { 0, 0, 0, 0 };

public: // 静的メンバ関数
	/// <summary>
	/// インスタンス取得
	/// </summary>
	/// <returns>インスタンス</returns>
	static SceneChange* GetInstance();

public: // メンバ関数
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize();

	/// <summary>
	/// 描画
	/// </summary>
	void Draw();

	/// <summary>
	/// 暗転
	/// </summary>
	/// <param name="time">速さ</param>
	/// <returns>暗転したか</returns>
	bool IsBlockOut(float time);

	/// <summary>
	/// 明るくなる
	/// </summary>
	/// <param name="time">速さ</param>
	/// <returns>明るくなったか</returns>
	bool IsBrighten(float time);

	/// <summary>
	/// 色をセット
	/// </summary>
	/// <param name="color">色</param>
	inline void SetColor(const DirectX::XMFLOAT4& color) { m_screenColor = color; };
};
