#include "Bullet.h"
#include "CollisionManager.h"
#include "CollisionAttribute.h"

#include <Helper.h>
#include <Random.h>

using namespace DirectX;

Bullet::Bullet(const XMFLOAT3& pos, const XMFLOAT3& vel, const XMFLOAT4& color, bool alive, bool homing, XMFLOAT3* targetPos)
{
	m_object.reset(Object3d::Create(3));
	m_booster.reset(ParticleManager::Create("Particle/FireParticle.png"));
	Initialize(pos, vel, color, alive, homing, targetPos);
}

Bullet::~Bullet()
{

}

void Bullet::Initialize(const XMFLOAT3& pos, const XMFLOAT3& vel, const XMFLOAT4& color, bool alive, bool homing, XMFLOAT3* targetPos)
{
	if (m_object == nullptr)
	{
		assert(0);
	}

	m_alive = alive;
	m_isHoming = homing;
	m_targetPos = targetPos;
	m_frame = 0;
	m_aliveTime = 300;

	m_pos = pos;
	if (m_isHoming == true && targetPos != nullptr)
	{
		float r = Helper::LengthFloat3(vel);
		XMFLOAT3 vec = vel;
		vec.x += Random::GetRanNum(0, 6) - 3;
		vec.y += Random::GetRanNum(1, 4);
		m_vel = Helper::NormalizeFloat3(vec);
		m_vel.x *= r;
		m_vel.y *= r;
		m_vel.z *= r;
	}
	else
	{
		m_vel = vel;
	}

	m_object->SetPosition(m_pos);
	m_object->SetScale({ 1.0f, 1.0f, 1.0f });
	m_object->SetColor(color);
	m_object->Update();
}

void Bullet::Update()
{
	if (m_alive == true)
	{
		if (m_aliveTime < m_frame)
		{
			m_alive = false;
		}
		else
		{
			m_frame++;
		}

		if (m_isHoming == true && m_targetPos != nullptr)
		{
			float r = Helper::LengthFloat3(m_vel);
			XMFLOAT3 acc = { 0, 0, 0 };
			acc.x = (m_targetPos->x - m_pos.x - m_vel.x * m_frame) * 2 / m_frame * m_frame;
			acc.y = (m_targetPos->y - m_pos.y - m_vel.y * m_frame) * 2 / m_frame * m_frame;
			acc.z = (m_targetPos->z - m_pos.z - m_vel.z * m_frame) * 2 / m_frame * m_frame;
			acc = Helper::NormalizeFloat3(acc);
			m_vel = Helper::NormalizeFloat3({ m_vel.x + acc.x, m_vel.y + acc.y, m_vel.z + acc.z });
			m_vel.x *= r;
			m_vel.y *= r;
			m_vel.z *= r;
			m_pos.x = m_pos.x + m_vel.x;
			m_pos.y = m_pos.y + m_vel.y;
			m_pos.z = m_pos.z + m_vel.z;

			m_booster->Add(60, m_pos, { 0, 0, 0 }, { 0, 0, 0 }, m_object->GetColor(), { 0.0f, 0.0f, 0.0f, 0.0f }, m_object->GetScale().x * 2, 6.0f, false);
		}
		else
		{
			m_pos.x = m_pos.x + m_vel.x;
			m_pos.y = m_pos.y + m_vel.y;
			m_pos.z = m_pos.z + m_vel.z;

			m_booster->Add(20, m_pos, { -m_vel.x / 10, -m_vel.y / 10, -m_vel.z / 10 }, { 0, 0, 0 }, m_object->GetColor(), { 0.0f, 0.0f, 0.0f, 0.0f }, m_object->GetScale().x * 2, 3.0f, true, &m_pos);
		}

		if (535 < fabs(m_pos.x))
		{
			m_alive = false;
		}
		else if (1100 < m_pos.z || m_pos.z < -360)
		{
			m_alive = false;
		}
		else if (m_pos.y < -12.0f)
		{
			m_alive = false;
		}

		m_object->SetPosition(m_pos);
	}
}

void Bullet::Draw()
{
	if (m_alive == true)
	{
		m_object->Draw();
	}
}

void Bullet::DrawEffect()
{
	m_booster->Draw();
}

bool Bullet::CheckCollision()
{
	Ray ray;
	ray.start = { m_pos.x, m_pos.y + 2.0f, m_pos.z, 0 };
	ray.start.m128_f32[1] += m_object->GetScale().y;
	ray.dir = { 0, -1, 0, 0 };

	RayCastHit rayHit;
	if (CollisionManager::GetInstance()->Raycast(ray, COLLISION_ATTR_LANDSHAPE, &rayHit, m_object->GetScale().y * 2.0f))
	{
		m_alive = false;

		return true;
	}

	return false;
}
