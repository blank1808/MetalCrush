#include "Enemy.h"
#include "SphereCollider.h"
#include "CollisionManager.h"
#include "CollisionAttribute.h"

#include <Random.h>
#include <Helper.h>

Enemy::Enemy(int HP)
{
	// モデルの読み込み
	m_enemyOBJ.reset(Object3d::Create(2));

	// 初期化
	Initialize(HP);
}

Enemy::~Enemy()
{
	m_enemyBullets.clear();
}

void Enemy::Initialize(int HP)
{
	// nullチェック
	if (m_enemyOBJ == nullptr)
	{
		assert(0);
	}

	// 基準座標
	XMFLOAT3 d_pos = { 220, 0, 650 };
	d_pos.x += static_cast<float>(Random::GetRanNum(0, 300) - 150);
	d_pos.z += static_cast<float>(Random::GetRanNum(0, 600) - 300);
	// 基準速度
	XMFLOAT3 d_vel = {};

	m_pos = d_pos;
	m_vel = { static_cast<float>(Random::GetRanNum(0, 4)) / 10, 0, static_cast<float>(Random::GetRanNum(0, 4)) / 10 };
	m_HP = HP;
	m_maxHP = HP;
	m_alive = true;
	m_bulletSpeed = 3;
	m_bulletInterval = 0;
	m_gravityTime = 0;

	// オブジェクト
	m_enemyOBJ->SetPosition(m_pos);
	SphereCollider* collider = new SphereCollider({ 0, 2, 0 }, 2);
	collider->SetAttribute(COLLISION_ATTR_ENEMYS);
	m_enemyOBJ->SetCollider(collider);
	m_enemyOBJ->Update();
}

void Enemy::Update()
{
	// エネミーの更新
	if (m_alive == true)
	{
		XMFLOAT3 acc = {};
		acc.y += (-0.98f * 0.25f) * powf(static_cast<float>(m_gravityTime) / 60, 2);

		// 重力時間
		m_gravityTime++;

		m_vel.x += acc.x;
		m_vel.y += acc.y;
		m_vel.y = min(m_vel.y, -2.0f);
		m_vel.z += acc.z;

		// 座標に速度を加算
		m_pos.x += m_vel.x;
		m_pos.y += m_vel.y;
		m_pos.z += m_vel.z;

		m_enemyOBJ->SetPosition(m_pos);

		// 弾の間隔
		m_bulletInterval++;
		if (90 < m_bulletInterval)
		{
			m_bulletInterval = 90;
		}

		// 当たり判定
		CheckCollision();

		// ダメージエフェクト
		DamageEffect();
	}

	// 弾の更新
	for (auto& m : m_enemyBullets)
	{
		m->Update();
	}

	// 球の削除
	if (m_enemyBullets.empty() == false)
	{
		m_enemyBullets.remove_if(
			[](Bullet* b)
			{
				return b->GetAlive() == false;
			}
		);
	}
}

void Enemy::Draw(ID3D12GraphicsCommandList* cmdList)
{
	// OBJオブジェクト描画
	Object3d::PreDraw(cmdList);
	
	if (m_alive == true)
	{
		m_enemyOBJ->Draw();
	}
	if (m_enemyBullets.empty() == false)
	{
		for (const auto& m : m_enemyBullets)
		{
			m->Draw();
		}
	}

	Object3d::PostDraw();
}

void Enemy::DrawEffect()
{
	for (const auto& m : m_enemyBullets)
	{
		m->DrawEffect();
	}
}

void Enemy::ShotBullet(const XMFLOAT3& targetPos)
{
	if (90 <= m_bulletInterval)
	{
		m_bulletInterval = 0;

		XMFLOAT3 vel = {};
		// 標的の座標を取得
		vel.x = targetPos.x - m_pos.x;
		vel.y = targetPos.y - m_pos.y;
		vel.z = targetPos.z - m_pos.z;

		// 長さを1にして10倍する
		float len = sqrtf(powf(vel.x, 2) + powf(vel.y, 2) + powf(vel.z, 2));
		vel.x = vel.x / len * m_bulletSpeed;
		vel.y = vel.y / len * m_bulletSpeed;
		vel.z = vel.z / len * m_bulletSpeed;

		if (CheckNoUsingBullet() == true)
		{
			for (auto& m : m_enemyBullets)
			{
				if (m->GetAlive() == false)
				{
					m->Initialize(m_pos, vel, { 0.8f, 0.4f, 0.2f, 0.8f }, true, false, nullptr);
					break;
				}
			}
		}
		else
		{
			m_enemyBullets.emplace_back(new Bullet(m_pos, vel, { 0.8f, 0.4f, 0.2f, 0.8f },  true));
		}
	}
}

bool Enemy::CheckNoUsingBullet()
{
	if (0 < m_enemyBullets.size())
	{
		// 使っていないのがあるか
		bool hit = false;
		for (const auto& m : m_enemyBullets)
		{
			if (m->GetAlive() == false)
			{
				hit = true;
				break;
			}
		}

		return hit;
	}
	else
	{
		return false;
	}
}

bool Enemy::SearchTarget(const XMFLOAT3& target)
{
	TurnTurret(target, true);

	// 範囲内なら
	if (Helper::LengthFloat3(m_pos, target) < 350.0f)
	{
		return true;
	}

	return false;
}

bool Enemy::EnemyDamage(int num)
{
	if (m_isdamage == false)
	{
		m_isdamage = true;
		m_damageTime = 0;
		m_HP -= num;
		if (m_HP <= 0)
		{
			m_HP = 0;
			return true;
		}
	}

	return false;
}

void Enemy::DamageEffect()
{
	if (m_isdamage == true)
	{
		m_damageTime++;
		if (60 < m_damageTime)
		{
			m_damageTime = 0;
			m_isdamage = false;
		}

		if (m_damageTime == 0 || m_damageTime % 5 == 0)
		{
			m_enemyOBJ->SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
		}
		else
		{
			m_enemyOBJ->SetColor({ 1.0f, 0.0f, 1.0f, 1.0f });
		}
	}
	else
	{
		if (m_HP <= m_maxHP / 3)
		{
			m_enemyOBJ->SetColor({ 1.0f, 0.0f, 0.0f, 1.0f });
		}
		else if (m_HP <= m_maxHP / 3 * 2)
		{
			m_enemyOBJ->SetColor({ 1.0f, 0.5f, 0.5f, 1.0f });
		}
		else
		{
			m_enemyOBJ->SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
		}
	}
}

void Enemy::TurnTurret(const XMFLOAT3& pos, bool isTarget)
{
	// 標的までのベクトル
	XMFLOAT2 vecA = {};
	if (isTarget)
	{
		vecA = { m_pos.x - pos.x, m_pos.z - pos.z };
	}
	else
	{
		vecA = { 0, 1 };
	}
	// 進行方向のベクトル
	XMFLOAT2 vecB = { m_vel.x, m_vel.z };
	// 内積
	float ips = (vecA.x * vecB.x) + (vecA.y * vecB.y);
	// 角度
	float div = Helper::LengthFloat2(vecA) * Helper::LengthFloat2(vecB);
	float r = DirectX::XMConvertToDegrees(acos(ips / div));
	if (vecB.x < 0)
	{
		r = -r;
	}

	XMFLOAT3 rot = m_enemyOBJ->GetRotation();
	if (rot.y < r - 1)
	{
		rot.y += 1;
	}
	else if (r + 1 < rot.y)
	{
		rot.y -= 1;
	}
	m_enemyOBJ->SetRotation(rot);
}

void Enemy::CheckCollision()
{
	SphereCollider* collider = dynamic_cast<SphereCollider*>(m_enemyOBJ->GetCollider());

	m_enemyOBJ->Update();

	Ray ray;
	ray.start = collider->center;
	ray.start.m128_f32[1] += collider->GetRadius();
	ray.dir = { 0, -1, 0, 0 };

	RayCastHit rayHit;
	if (CollisionManager::GetInstance()->Raycast(ray, COLLISION_ATTR_LANDSHAPE, &rayHit, collider->GetRadius() * 2.0f))
	{
		m_pos.y -= (rayHit.distance - collider->GetRadius() * 2);
		m_vel.y = 0;
		m_enemyOBJ->SetPosition(m_pos);
		m_gravityTime = 0;
	}

	if (530 < fabs(m_pos.x))
	{
		m_vel.x = -m_vel.x;
	}
	if (1095 < m_pos.z || m_pos.z < -355)
	{
		m_vel.z = -m_vel.z;
	}
	if (m_pos.y < -12.0f)
	{
		m_pos.y = 0.0f;
	}
}
