#include "DirectXCommon.h"
#include "Model.h"
#include "Player.h"
#include "SphereCollider.h"
#include "CollisionManager.h"
#include "CollisionAttribute.h"

#include <Helper.h>
#include <Random.h>

using namespace DirectX;

Player::Player()
{
	// モデルの読み込み
	m_playerLeg.reset(Object3d::Create(4));
	m_playerBody.reset(Object3d::Create(5));
	m_playerArm.reset(Object3d::Create(6));
	m_booster.reset(ParticleManager::Create("Particle/FireParticle.png"));

	// 初期化
	Initialize();
}

Player::~Player()
{
	m_playerBullets.clear();
}

void Player::Initialize()
{
	// nullチェック
	if (m_playerLeg == nullptr || m_booster == nullptr)
	{
		assert(0);
	}

	// プレイヤー
	m_player.isAlive = true;
	m_player.isDash = false;
	m_player.isFlying = true;
	m_player.boostGauge = 360;
	m_player.HP = 20;
	m_player.pos = { 0, 20, -100 };
	m_player.vel = { 0, 0, 0 };

	// 通常移動
	m_move.acc = 0.05f;
	m_move.dec = 0.7f;
	m_move.maxSpeed = 0.75f;

	// ジャンプ
	m_jump.acc = 0.05f;
	m_jump.maxVelY = 1.0f;

	// ダッシュ
	m_dash.acc = 0;
	m_dash.time = 0;
	m_dash.DT = 8.0f;

	// 重力時間
	m_gravityTime = 0;

	// バレット
	m_normalBullet.targetPos = { 0, 0, 0 };
	m_normalBullet.maxNum = 20;
	m_normalBullet.num = m_normalBullet.maxNum;
	m_normalBullet.speed = 6;
	m_normalBullet.interval = 0;
	m_normalBullet.reloadTimer = 0;
	m_normalBullet.isReload = false;
	m_normalBullet.isLock = false;

	m_homingBullet.targetPos = { 0, 0, 0 };
	m_homingBullet.maxNum = 4;
	m_homingBullet.num = m_homingBullet.maxNum;
	m_homingBullet.speed = 6;
	m_homingBullet.interval = 0;
	m_homingBullet.reloadTimer = 0;
	m_homingBullet.isReload = false;
	m_homingBullet.isLock = false;

	// ブースター
	m_gauge.coolTime = 0;
	m_gauge.add = 2;
	m_gauge.dashDec = 150;
	m_gauge.jumpDec = m_gauge.add * 3;
	m_gauge.max = m_player.boostGauge;

	// ブースターオフセット
	for (int i = 0; i < 2; i++)
	{
		m_offset.main[i] =XMFLOAT3(0, 0, 0);
		m_offset.main[i] = XMFLOAT3(0, 0, 0);
		m_offset.back[i] = XMFLOAT3(0, 0, 0);
	}
	XMFLOAT3 mOffset = { 0.1f, 0.15f, -0.55f };
	m_offset.mainOffset[0] = { -mOffset.x * m_playerLeg->GetScale().x, mOffset.y * m_playerLeg->GetScale().y, mOffset.z * m_playerLeg->GetScale().z};
	m_offset.mainOffset[1] = { mOffset.x * m_playerLeg->GetScale().x, mOffset.y * m_playerLeg->GetScale().y, mOffset.z * m_playerLeg->GetScale().z };
	XMFLOAT3 sOffset = { 0.65f, 0.55f, 0 };
	m_offset.sideOffset[0] = { sOffset.x * m_playerLeg->GetScale().x, sOffset.y * m_playerLeg->GetScale().y, sOffset.z * m_playerLeg->GetScale().z };
	m_offset.sideOffset[1] = { -sOffset.x * m_playerLeg->GetScale().x, sOffset.y * m_playerLeg->GetScale().y, sOffset.z * m_playerLeg->GetScale().z };
	XMFLOAT3 bOffset = { 0.25f, 0, 0.25f };
	m_offset.backOffset[0] = { -bOffset.x * m_playerLeg->GetScale().x, bOffset.y * m_playerLeg->GetScale().z, bOffset.z * m_playerLeg->GetScale().y };
	m_offset.backOffset[1] = { bOffset.x * m_playerLeg->GetScale().x, bOffset.y * m_playerLeg->GetScale().z, bOffset.z * m_playerLeg->GetScale().y };

	// カメラ
	m_camera.addAngle = 1.5f;

	// オブジェクト
	m_playerLeg->SetPosition(m_player.pos);
	m_playerLeg->SetScale({ 0.25f, 0.25f, 0.25f });
	m_playerLeg->SetRotation({ 0, -90, 0 });
	SphereCollider *collider = new SphereCollider({ 0, 1, 0 }, 2);
	collider->SetAttribute(COLLISION_ATTR_ALLIES);
	m_playerLeg->SetCollider(collider);
	m_playerLeg->Update();
	m_playerBody->SetParent(m_playerLeg.get());
	m_playerBody->SetPosition({ 0, 1.5f, 0 });
	m_playerBody->Update();
	m_playerArm->SetParent(m_playerBody.get());
	m_playerArm->SetPosition({ 0, 0.75f, 0 });
	m_playerArm->SetRotation({ 0, 0, 90 });
	m_playerArm->Update();
}

void Player::Update()
{
	// 加速度
	XMFLOAT3 acc = {};	

	// 生きているなら
	if (m_player.isAlive == true)
	{
		// 縦横移動
		MovePlayer(acc);

		// 上下の移動
		JumpPlayer(acc);

		// 重力
		acc.y += (-0.98f * 0.25f) * powf(static_cast<float>(m_gravityTime) / 60, 2);

		// 重力時間
		m_gravityTime++;

		// 速度に加速を加算
		NormalizeVel(acc);

		// ダッシュ
		DashPlayer();

		// 座標のセット
		SetObjectPosition();

		// ショット
		BulletUpdate();

		// オフセットの更新
		UpdateOffset();

		if (m_player.isDash == false)
		{
			// メインブースターのパーティクル
			MainBooster();

			// サイドブースターのパーティクル
			SideBooster();

			// バックブースターのパーティクル
			BacktBooster();
		}
		else
		{
			// ダッシュ時のパーティクル
			DashBooster(acc);
		}

		// ブーストゲージ
		BoosterGauge();
	}

	// 弾の更新
	for (const auto& m : m_playerBullets)
	{
		m->Update();
	}

	// カメラワーク
	CameraWork();
}

void Player::Draw(ID3D12GraphicsCommandList* cmdList)
{
	// OBJオブジェクト描画
	Object3d::PreDraw(cmdList);
	
	for (const auto m : m_playerBullets)
	{
		m->Draw();
	}
	if (m_player.isAlive == true)
	{
		m_playerLeg->Draw();
		m_playerBody->Draw();
		m_playerArm->Draw();
	}
	
	Object3d::PostDraw();
}

void Player::DrawEffect()
{
	// パーティクル描画
	m_booster->Draw();
	for (const auto m : m_playerBullets)
	{
		m->DrawEffect();
	}
}

void Player::MovePlayer(XMFLOAT3& acc)
{
	if (Input::GetInstance()->LeftStickAngle().x != 0 || Input::GetInstance()->LeftStickAngle().y != 0)
	{
		// 加速
		acc.x += Input::GetInstance()->LeftStickAngle().x * m_move.acc;
		acc.z += Input::GetInstance()->LeftStickAngle().y * m_move.acc;
	}
	else
	{
		// 減速
		m_player.vel.x *= m_move.dec;
		m_player.vel.z *= m_move.dec;

		// 一定以下なら0に
		if (fabs(m_player.vel.x) < 0.01f)
		{
			m_player.vel.x = 0;
		}
		if (fabs(m_player.vel.z) < 0.01f)
		{
			m_player.vel.z = 0;
		}
	}
}

void Player::JumpPlayer(XMFLOAT3& acc)
{
	// ジャンプ
	if (Input::GetInstance()->PullLeftTrigger() && m_gauge.coolTime == 0)
	{
		acc.y += m_jump.acc;
		m_gravityTime = 0;
		m_player.isFlying = true;
		m_player.boostGauge -= m_gauge.jumpDec;
		if (m_player.boostGauge < 0)
		{
			m_gauge.coolTime = 1;
			m_player.boostGauge = 0;
		}
	}
}

void Player::DashPlayer()
{
	// ダッシュ
	if(Input::GetInstance()->SwitchRightTrigger() && m_player.isDash == false && m_player.vel.x != 0 && m_player.vel.z != 0 && m_gauge.coolTime == 0)
	{
		m_player.isDash = true;
		m_player.boostGauge -= m_gauge.dashDec;
		if (m_player.boostGauge < 0)
		{
			m_gauge.coolTime = 1;
			m_player.boostGauge = 0;
		}
	}

	// 加速
	if (m_player.isDash == true)
	{
		m_dash.time += m_dash.DT;

		if (60 <= m_dash.time)
		{
			m_dash.time = 60;
			m_dash.DT = -3.0f;
		}
		else if (m_dash.time <= 0)
		{
			m_dash.time = 0;
			m_dash.DT = 8.0f;
			m_player.isDash = false;
		}

		float tmp_t = 0;
		if (0 < fabs(m_dash.DT))
		{
			tmp_t = (m_dash.time / 60) * (2 - (m_dash.time) / 60);
		}

		m_dash.acc = 5.0f * tmp_t;

		m_player.vel.x *= 1 + m_dash.acc;
		m_player.vel.z *= 1 + m_dash.acc;
	}
}

void Player::NormalizeVel(const XMFLOAT3& acc)
{
	m_player.vel.x += acc.x;
	m_player.vel.y += acc.y;
	m_player.vel.z += acc.z;
	XMFLOAT3 vec2 = { m_player.vel.x , 0 ,m_player.vel.z };
	if (m_move.maxSpeed < Helper::LengthFloat3(vec2))
	{
		vec2 = Helper::NormalizeFloat3(vec2);
		m_player.vel.x = vec2.x * m_move.maxSpeed;
		m_player.vel.z = vec2.z * m_move.maxSpeed;
	}
	m_player.vel.y = Helper::Clamp(-2.0f, m_player.vel.y, m_jump.maxVelY);
}

void Player::SetObjectPosition()
{
	// 速度のベクトル
	m_player.pos = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_player.vel);

	// 地面に当たっいるか
	CheckCollision();

	// 座標セット
	m_playerLeg->SetPosition(m_player.pos);
}

void Player::BulletUpdate()
{
	// 発射
	if (15 < m_normalBullet.interval && m_normalBullet.isReload == false && Input::GetInstance()->PushButton(BUTTON::RB) == true)
	{
		m_normalBullet.interval = 0;

		XMFLOAT3 vel = {};
		if (m_normalBullet.isLock == true)
		{
			// 標的までの方向を取得
			vel.x = m_normalBullet.targetPos.x - m_player.pos.x;
			vel.y = m_normalBullet.targetPos.y - m_player.pos.y;
			vel.z = m_normalBullet.targetPos.z - m_player.pos.z;

			// 長さを正規化する
			vel = Helper::NormalizeFloat3(vel);
			vel.x *= m_normalBullet.speed;
			vel.y *= m_normalBullet.speed;
			vel.z *= m_normalBullet.speed;
		}
		else
		{
			// まっすぐ飛ばす
			vel = { 0, 0, m_normalBullet.speed };

			// カメラの方向に変換
			vel = Camera::GetInstance()->ConvertWindowXYPos({ 0, 0, 0 }, vel);
		}

		m_playerBullets.emplace_back(new Bullet(m_player.pos, vel, { 0.2f, 0.4f, 0.8f, 0.8f }, true, false, &m_normalBullet.targetPos));
		m_normalBullet.num--;

		if (m_normalBullet.num <= 0)
		{
			m_normalBullet.num = m_normalBullet.maxNum;
			m_normalBullet.isReload = true;
		}
	}
	else
	{
		// 発射間隔の更新
		m_normalBullet.interval++;
	}
	// リロード
	if (m_normalBullet.isReload == true)
	{
		m_normalBullet.reloadTimer++;
		if (180 < m_normalBullet.reloadTimer)
		{
			m_normalBullet.reloadTimer = 0;
			m_normalBullet.isReload = false;
		}
	}

	// 発射
	if (15 < m_homingBullet.interval && m_homingBullet.isReload == false && Input::GetInstance()->PushButton(BUTTON::LB) == true && m_homingBullet.isLock == true)
	{
		m_homingBullet.interval = 0;

		XMFLOAT3 vel = {};
		// 標的までの方向を取得
		vel.x = m_homingBullet.targetPos.x - m_player.pos.x;
		vel.y = m_homingBullet.targetPos.y - m_player.pos.y;
		vel.z = m_homingBullet.targetPos.z - m_player.pos.z;

		// 長さを正規化する
		vel = Helper::NormalizeFloat3(vel);
		vel.x *= m_homingBullet.speed;
		vel.y *= m_homingBullet.speed;
		vel.z *= m_homingBullet.speed;

		m_playerBullets.emplace_back(new Bullet(m_player.pos, vel, { 0.2f, 0.4f, 0.8f, 0.8f }, true, true, &m_homingBullet.targetPos));
		m_homingBullet.num--;

		if (m_homingBullet.num <= 0)
		{
			m_homingBullet.num = m_homingBullet.maxNum;
			m_homingBullet.isReload = true;
		}
	}
	else
	{
		// 発射間隔の更新
		m_homingBullet.interval++;
	}
	// リロード
	if (m_homingBullet.isReload == true)
	{
		m_homingBullet.reloadTimer++;
		if (180 < m_homingBullet.reloadTimer)
		{
			m_homingBullet.reloadTimer = 0;
			m_homingBullet.isReload = false;
		}
	}

	// 球の削除
	if (m_playerBullets.empty() == false)
	{
		m_playerBullets.remove_if(
			[](Bullet* b)
			{
				return b->GetAlive() == false;
			}
		);
	}
}

void Player::CameraWork()
{
	// 追従カメラ
	XMFLOAT3 cameraAngle = {};
	if (Input::GetInstance()->RightStickAngle().x != 0 || Input::GetInstance()->RightStickAngle().y != 0)
	{
		cameraAngle.y += Input::GetInstance()->RightStickAngle().x * m_camera.addAngle;
		cameraAngle.x -= Input::GetInstance()->RightStickAngle().y * m_camera.addAngle;
	}

	// 焦点座標
	XMFLOAT3 tPos = m_player.pos;
	XMFLOAT3 tVel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, m_player.vel);
	tPos.x -= tVel.x;
	tPos.y -= tVel.y;
	tPos.z -= tVel.z;
	Camera::GetInstance()->FollowUpCamera(tPos, Camera::GetInstance()->GetEyeDistance(), cameraAngle);

	// 姿勢制御
	XMFLOAT3 tmp_rot = m_playerLeg->GetRotation();
	tmp_rot.y += cameraAngle.y;
	m_playerLeg->SetRotation(tmp_rot);
}

void Player::UpdateOffset()
{
	m_offset.main[0] = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_offset.mainOffset[0]);
	m_offset.main[1] = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_offset.mainOffset[1]);
	m_offset.side[0] = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_offset.sideOffset[0]);
	m_offset.side[1] = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_offset.sideOffset[1]);
	m_offset.back[0] = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_offset.backOffset[0]);
	m_offset.back[1] = Camera::GetInstance()->ConvertWindowYPos(m_player.pos, m_offset.backOffset[1]);
}

void Player::MainBooster()
{
	if (0 < m_player.vel.y || 0 < m_player.vel.z)
	{
		for (int i = 0; i < 2; i++)
		{
			XMFLOAT3 tmp_vel = Helper::NormalizeFloat3(m_player.vel);
			tmp_vel.x = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			if (0 < tmp_vel.y)
			{
				tmp_vel.y = -fabs(tmp_vel.y) * 0.1f + static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			}
			else
			{
				tmp_vel.y = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			}
			if (0 < tmp_vel.z)
			{
				tmp_vel.z = -fabs(tmp_vel.z) * 0.1f + static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			}
			else
			{
				tmp_vel.z = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			}
			tmp_vel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, tmp_vel);

			m_booster->Add(5, m_offset.main[0], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.15f, true, &m_offset.main[0]);
			m_booster->Add(5, m_offset.main[1], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.15f, true, &m_offset.main[1]);
		}
	}
}

void Player::SideBooster()
{
	if (0.2f < fabs(m_player.vel.x))
	{
		for (int i = 0; i < 2; i++)
		{
			XMFLOAT3 tmp_vel = Helper::NormalizeFloat3(m_player.vel);
			tmp_vel.x = -(tmp_vel.x * 0.1f) + static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			tmp_vel.y = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			tmp_vel.z = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			tmp_vel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, tmp_vel);

			if (m_player.vel.x < 0)
			{
				m_booster->Add(5, m_offset.side[0], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.15f, true, &m_offset.side[0]);
			}
			else
			{
				m_booster->Add(5, m_offset.side[1], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.15f, true, &m_offset.side[1]);
			}
		}
	}
}

void Player::BacktBooster()
{
	if (m_player.vel.z < 0)
	{
		for (int i = 0; i < 2; i++)
		{
			XMFLOAT3 tmp_vel = Helper::NormalizeFloat3(m_player.vel);
			tmp_vel.x = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			tmp_vel.y = static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			tmp_vel.z = fabs(tmp_vel.z) * 0.1f + static_cast<float>(Random::GetRanNum(0, 2) - 1) / 100;
			tmp_vel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, tmp_vel);

			m_booster->Add(5, m_offset.back[0], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.15f, true, &m_offset.back[0]);
			m_booster->Add(5, m_offset.back[1], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.15f, true, &m_offset.back[1]);
		}
	}
}

void Player::DashBooster(const XMFLOAT3& acc)
{
	if (acc.x != 0 || acc.z != 0)
	{
		if (fabs(acc.x) < fabs(acc.z))
		{
			if (0 < acc.z)
			{
				for (int i = 0; i < 10; i++)
				{
					XMFLOAT3 tmp_vel = Helper::NormalizeFloat3(m_player.vel);
					tmp_vel.x = static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
					tmp_vel.y = static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
					tmp_vel.z = -fabs(tmp_vel.z) * 0.4f + static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
					tmp_vel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, tmp_vel);

					m_booster->Add(5, m_offset.main[0], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.9f, true, &m_offset.main[0]);
					m_booster->Add(5, m_offset.main[1], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.9f, true, &m_offset.main[1]);
				}
			}
			else
			{
				for (int i = 0; i < 10; i++)
				{
					XMFLOAT3 tmp_vel = Helper::NormalizeFloat3(m_player.vel);
					tmp_vel.x = static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
					tmp_vel.y = static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
					tmp_vel.z = fabs(tmp_vel.z) * 0.4f + static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
					tmp_vel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, tmp_vel);

					m_booster->Add(5, m_offset.back[0], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.9f, true, &m_offset.back[0]);
					m_booster->Add(5, m_offset.back[1], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.9f, true, &m_offset.back[1]);
				}
			}
		}
		else
		{
			for (int i = 0; i < 10; i++)
			{
				XMFLOAT3 tmp_vel = Helper::NormalizeFloat3(m_player.vel);
				tmp_vel.x = -(tmp_vel.x * 0.4f) + static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
				tmp_vel.y = static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
				tmp_vel.z = static_cast<float>(Random::GetRanNum(0, 6) - 3) / 50;
				tmp_vel = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, tmp_vel);

				if (m_player.vel.x < 0)
				{
					m_booster->Add(5, m_offset.side[0], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.9f, true, &m_offset.side[0]);
				}
				else if (0 < m_player.vel.x)
				{
					m_booster->Add(5, m_offset.side[1], tmp_vel, { 0, 0, 0 }, { 1.0f, 0.6f, 0.4f, 1.0f }, { 0.0f, 0.2f, 0.4f, 0.0f }, 0.3f, 0.9f, true, &m_offset.side[1]);
				}
			}
		}
	}
}

void Player::BoosterGauge()
{
	// クールタイム出ないなら回復
	if (m_gauge.coolTime == 0)
	{
		m_player.boostGauge += m_gauge.add;
	}
	else
	{
		m_gauge.coolTime++;
		if (60 * 2 <= m_gauge.coolTime)
		{
			m_gauge.coolTime = 0;
		}
	}

	if (m_gauge.max < m_player.boostGauge)
	{
		m_player.boostGauge = m_gauge.max;
	}
}

void Player::CheckCollision()
{
	if (535 < fabs(m_player.pos.x))
	{
		m_player.pos.x = Helper::Clamp(-535, m_player.pos.x, 535);
		m_player.vel.x = 0;
	}
	if (1100 < m_player.pos.z || m_player.pos.z < -360)
	{
		m_player.pos.z = Helper::Clamp(-360, m_player.pos.z, 1100);
		m_player.vel.z = 0;
	}
	if (m_player.pos.y < -12.0f)
	{
		m_player.pos.y = -12.0f;
		m_player.vel.y = 0;
	}

	SphereCollider* collider = dynamic_cast<SphereCollider*>(m_playerLeg->GetCollider());

	Ray ray;
	ray.start = collider->center;
	ray.start.m128_f32[1] += collider->GetRadius();
	ray.dir = { 0, -1, 0, 0 };

	float adsDistance = 0.0f;
	if (m_player.isFlying == false)
	{
		adsDistance = 1.0f;
	}

	RayCastHit rayHit;
	if (CollisionManager::GetInstance()->Raycast(ray, COLLISION_ATTR_LANDSHAPE, &rayHit, collider->GetRadius() * 2.0f + adsDistance))
	{
		m_player.pos.y -= (rayHit.distance - collider->GetRadius() * 2);
		m_player.vel.y = 0;
		m_gravityTime = 0;
	}
}

bool Player::SearchTarget(const XMFLOAT3& target)
{
	// 範囲内なら
	if (Helper::LengthFloat3(m_player.pos, target) < 250.0f)
	{
		return true;
	}

	return false;
}
