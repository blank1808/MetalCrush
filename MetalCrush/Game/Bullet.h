#pragma once
#include "Model.h"
#include "Object3d.h"
#include "ParticleManager.h"

#include <DirectXMath.h>
#include <memory>

using namespace std;

/// <summary>
/// 弾
/// </summary>
class Bullet
{
public: // エイリアス
   // DirectX::を省略
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // メンバ変数
	// オブジェクト
	std::shared_ptr<Object3d> m_object;
	// 座標
	XMFLOAT3 m_pos = { 0, 0, 0 };
	// 速度
	XMFLOAT3 m_vel = { 0, 0, 0 };
	// 生存フラグ
	bool m_alive = false;
	// 追尾型するか
	bool m_isHoming = false;
	// 追尾対象の座標
	XMFLOAT3* m_targetPos;
	// フレーム
	int m_frame = 0;
	// 生存時間
	int m_aliveTime = 0;

	// パーティクル
	std::shared_ptr<ParticleManager> m_booster;

public: // メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Bullet(const XMFLOAT3& pos = { 0, 0, 0 }, const XMFLOAT3& vel = { 0, 0, 0 }, const XMFLOAT4& color = { 1, 1, 1, 1 }, bool alive = true, bool homing = false, XMFLOAT3* targetPos = nullptr);

	/// <summary>
	/// デストラクタ
	/// </summary>
	~Bullet();

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="pos">座標</param>
	/// <param name="vec">ベクトル</param>
	/// <param name="color">色</param>
	/// <param name="alive">生存フラグ</param>
	/// <param name="homing">追尾するか</param>
	void Initialize(const XMFLOAT3& pos, const XMFLOAT3& vel, const XMFLOAT4& color, bool alive, bool homing, XMFLOAT3* targetPos);

	/// <summary>
	///  更新
	/// </summary>
	void Update();

	/// <summary>
	/// 描画
	/// </summary>
	void Draw();

	/// <summary>
	/// エフェクト描画
	/// </summary>
	void DrawEffect();

public: // メンバ関数
	/// <summary>
	/// 当たり判定
	/// </summary>
	bool CheckCollision();

public: // アクセッサ
	/// <summary>
	/// 座標を取得
	/// </summary>
	/// <returns>座標</returns>
	inline const XMFLOAT3 GetPosition() { return m_pos; }

	/// <summary>
	/// 座標をセット
	/// </summary>
	/// <param name="pos">座標</param>
	inline void SetPosition(const XMFLOAT3& pos)
	{
		m_pos = pos;

		m_object->SetPosition(m_pos);
	}

	/// <summary>
	/// 速度を取得
	/// </summary>
	/// <returns>ベクトル</returns>
	inline const XMFLOAT3 GetVelocity() { return m_vel; }

	/// <summary>
	/// 速度をセット
	/// </summary>
	/// <param name="vel">ベクトル</param>
	inline void SetVelocity(const XMFLOAT3& vel)
	{
		m_vel = vel;
	}

	/// <summary>
	/// 生存フラグを取得
	/// </summary>
	/// <returns>生存フラグ</returns>
	inline const bool GetAlive() { return m_alive; }

	/// <summary>
	/// 生存フラグをセット
	/// </summary>
	/// <param name="alive">生存フラグ</param>
	inline void SetAlive(const bool alive)
	{
		m_alive = alive;
	}

	/// <summary>
	/// オブジェクトの取得
	/// </summary>
	/// <returns>オブジェクト</returns>
	inline Object3d* GetObject3d() { return m_object.get(); };
};
