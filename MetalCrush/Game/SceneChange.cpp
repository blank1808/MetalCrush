#include "SceneChange.h"

SceneChange* SceneChange::GetInstance()
{
	static SceneChange scene;

	return &scene;
}

void SceneChange::Initialize()
{
	m_shadowScreen.reset(Sprite::Create(100));
	m_shadowScreen->SetSize({ 1280, 720 });
	m_shadowScreen->SetColor(m_screenColor);
}

void SceneChange::Draw()
{
	m_shadowScreen->Draw();
}

bool SceneChange::IsBlockOut(float time)
{
	if (m_screenColor.w < 1.0f)
	{
		m_screenColor.w += 0.1f * time;
		m_screenColor.w = min(m_screenColor.w, 1.0f);

		m_shadowScreen->SetColor(m_screenColor);

		return false;
	}

	return true;
}

bool SceneChange::IsBrighten(float time)
{
	if (0 < m_screenColor.w)
	{
		m_screenColor.w -= 0.1f * time;
		m_screenColor.w = max(m_screenColor.w, 0);

		m_shadowScreen->SetColor(m_screenColor);

		return false;
	}

	return true;
}
