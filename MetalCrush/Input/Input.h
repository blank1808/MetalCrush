#pragma once
#include "WinApp.h"

#include <windows.h>
#include <dinput.h>
#include <wrl.h>
#include <memory>
#include <DirectXMath.h>
#include <Xinput.h>

/// <summary>
/// ゲームパッド配置（XBOX）
/// </summary>
enum BUTTON
{
	// 十字
	U_CROSS = 0x0001,
	D_CROSS = 0x0002,
	L_CROSS = 0x0004,
	R_CROSS = 0x0008,
	// 中央右
	START = 0x0010,
	// 中央左
	BACK = 0x0020,
	// スティック
	L_STICK = 0x0040,
	R_STICK = 0x0080,
	// ボタン
	LB = 0x0100,
	RB = 0x0200,
	A = 0x1000,
	B = 0x2000,
	X = 0x4000,
	Y = 0x8000,
};

/// <summary>
/// マウス配置
/// </summary>
enum MOUSE
{
	LEFT, RIGHT, MIDDLE
};

/// <summary>
/// 入力処理
/// </summary>
class Input
{
public: // エイリアス
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	using XMFLOAT2 = DirectX::XMFLOAT2;

private: // 定数
	// Direct Inputのバージョン指定
	constexpr static int s_dInputVersion = 0x0800;

private: // メンバ変数
	// DirectInputのインスタンス
	ComPtr<IDirectInput8> m_dInput;
	// キーボードデバイス
	ComPtr<IDirectInputDevice8> m_devKeyboard;
	// キー判定
	BYTE m_keys[256] = {};
	// 前フレームのキー情報
	BYTE m_oldKeys[256] = {};
	// マウスデバイス
	ComPtr<IDirectInputDevice8> m_devMouse;
	// マウス判定
	DIMOUSESTATE2 m_mouseState = {};
	// 前フレームのマウス判定
	DIMOUSESTATE2 m_oldMouseState = {};
	// ゲームパッドの判定
	XINPUT_STATE m_gamePadState = {};
	// 前フレームのゲームパッドの判定
	XINPUT_STATE m_oldGamePadState = {};
	// コントローラーを取得しているか
	bool m_isController = false;

public: // 静的メンバ関数
	/// <summary>
	/// インスタンスを取得
	/// </summary>
	/// <returns>インスタンス</returns>
	static Input* GetInstance();

public: // メンバ関数
	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize(WinApp* win);

	/// <summary>
	/// 更新
	/// </summary>
	void Update();

	/// <summary>
	/// キー入力
	/// </summary>
	/// <param name="key">押しているキー</param>
	/// <returns>押しているか</returns>
	bool PushKey(const BYTE key);

	/// <summary>
	/// キー入力（長押し不可）
	/// </summary>
	/// <param name="key">押しているキー</param>
	/// <returns>押しているか</returns>
	bool TriggerKey(const BYTE key);

	/// <summary>
	/// 左クリック
	/// </summary>
	/// <param name="Mouse">押しているボタン</param>
	/// <returns>押しているか</returns>
	bool PushMouse(const int Mouse);

	/// <summary>
	/// 右クリック
	/// </summary>
	/// <param name="Mouse">押しているボタン</param>
	/// <returns>押しているか</returns>
	bool TriggerMouse(const int Mouse);
	
	/// <summary>
	/// ゲームパッド左スティックを倒した割合
	/// </summary>
	/// <returns>スティックを倒した割合</returns>
	XMFLOAT2 LeftStickAngle();

	/// <summary>
	/// ゲームパッド左スティックを倒した割合
	/// </summary>
	/// <returns>スティックを倒した割合</returns>
	XMFLOAT2 RightStickAngle();

	/// <summary>
	/// ゲームパッドボタン
	/// </summary>
	/// <param name="Button">押しているボタン</param>
	/// <returns>押しているか</returns>
	bool PushButton(const int Button);

	/// <summary>
	/// ゲームパッドボタン（長押し不可）
	/// </summary>
	/// <param name="Button">押しているボタン</param>
	/// <returns>押しているか</returns>
	bool TriggerButton(const int Button);

	/// <summary>
	/// 左トリガーを引いているか
	/// </summary>
	/// <returns>引いているか否か</returns>
	bool PullLeftTrigger();

	/// <summary>
	/// 右トリガーを引いているか
	/// </summary>
	/// <returns>引いているか否か</returns>
	bool PullRightTrigger();

	/// <summary>
	/// 左トリガーを引いているか（長押し不可）
	/// </summary>
	/// <returns>引いているか否か</returns>
	bool SwitchLeftTrigger();

	/// <summary>
	/// 右トリガーを引いているか（長押し不可）
	/// </summary>
	/// <returns>引いているか否か</returns>
	bool SwitchRightTrigger();

	/// <summary>
	/// コントローラーの情報を取得しているか
	/// </summary>
	/// <returns></returns>
	inline bool GetIsController() { return m_isController; }
};