#include "Input.h"

#include <assert.h>

using namespace DirectX;

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")
#pragma comment (lib, "xinput.lib")

Input* Input::GetInstance()
{
	static Input input;

	return &input;
}

void Input::Initialize(WinApp* win)
{
	HRESULT result;

	//インターフェース作成
	result = DirectInput8Create(
		win->GetWindowInstance(), static_cast<DWORD>(s_dInputVersion), IID_IDirectInput8, (void**)&m_dInput, nullptr);
	if (FAILED(result))
	{
		assert(0);
	}

	//キーボードデバイスの生成
	m_devKeyboard = nullptr;
	result = m_dInput->CreateDevice(GUID_SysKeyboard, &m_devKeyboard, NULL);

	//マウスの生成
	m_devMouse = nullptr;
	result = m_dInput->CreateDevice(GUID_SysMouse, &m_devMouse, NULL);

	// キーボード
	if (m_devKeyboard != nullptr)
	{
		//入力データ形式のセット
		result = m_devKeyboard->SetDataFormat(&c_dfDIKeyboard);
		if (FAILED(result))
		{
			assert(0);
		}

		//排他制御レベルセット
		result = m_devKeyboard->SetCooperativeLevel(win->GetHWND(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE | DISCL_NOWINKEY);
		if (FAILED(result))
		{
			assert(0);
		}
	}

	// マウス
	if (m_devMouse != nullptr)
	{
		//入力データ形式のセット
		result = m_devMouse->SetDataFormat(&c_dfDIMouse2);
		if (FAILED(result))
		{
			assert(0);
		}

		//排他制御レベルセット
		result = m_devMouse->SetCooperativeLevel(win->GetHWND(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE | DISCL_NOWINKEY);
		if (FAILED(result))
		{
			assert(0);
		}
	}

	Update();
}

void Input::Update()
{
	HRESULT result = S_FALSE;

	if (m_devKeyboard != nullptr)
	{
		//キーボード情報の取得開始
		result = m_devKeyboard->Acquire();
		//前フレームのキー情報取得
		memcpy(m_oldKeys, m_keys, sizeof(m_keys));
		//全キーの情報を取得する
		result = m_devKeyboard->GetDeviceState(sizeof(m_keys), m_keys);
	}

	if (m_devMouse != nullptr)
	{
		//マウス情報の取得開始
		result = m_devMouse->Acquire();
		//前フレームの判定
		m_oldMouseState = m_mouseState;
		//全クリックの情報を取得する
		result = m_devMouse->GetDeviceState(sizeof(m_mouseState), &m_mouseState);
	}

	m_oldGamePadState = m_gamePadState;
	DWORD dwResult = XInputGetState(0, &m_gamePadState);
	if (dwResult == 0)
	{
		m_isController = true;
	}
	else
	{
		m_isController = false;
	}
}

bool Input::PushKey(const BYTE key)
{
	assert(0 <= key && key < 256);

	// 押しているか
	if (m_keys[key])
	{
		return true;
	}
	return false;
}

bool Input::TriggerKey(const BYTE key)
{
	assert(0 <= key && key < 256);

	// 押しているか
	if (m_keys[key] && !m_oldKeys[key])
	{
		return true;
	}
	return false;
}

bool Input::PushMouse(const int Mouse)
{
	assert(0 <= Mouse && Mouse < 3);

	// 押しているか
	if (m_mouseState.rgbButtons[Mouse])
	{
		return true;
	}

	return false;
}

bool Input::TriggerMouse(const int Mouse)
{
	assert(0 <= Mouse && Mouse < 3);

	// 押しているか
	if (m_mouseState.rgbButtons[Mouse] && !m_oldMouseState.rgbButtons[Mouse])
	{
		return true;
	}

	return false;
}

XMFLOAT2 Input::LeftStickAngle()
{
	// デットゾーンの判定
	if ((m_gamePadState.Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && m_gamePadState.Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) &&
		 (m_gamePadState.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && m_gamePadState.Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
	{
		return XMFLOAT2(0, 0);
	}
	else
	{
		XMFLOAT2 result = { static_cast<float>(m_gamePadState.Gamepad.sThumbLX) / 32768, static_cast<float>(m_gamePadState.Gamepad.sThumbLY) / 32768 };

		return result;
	}
}

XMFLOAT2 Input::RightStickAngle()
{
	// デットゾーンの判定
	if ((m_gamePadState.Gamepad.sThumbRX < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && m_gamePadState.Gamepad.sThumbRX > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE) &&
		(m_gamePadState.Gamepad.sThumbRY < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && m_gamePadState.Gamepad.sThumbRY > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE))
	{
		return XMFLOAT2(0, 0);
	}
	else
	{
		XMFLOAT2 result = { static_cast<float>(m_gamePadState.Gamepad.sThumbRX) / 32768, static_cast<float>(m_gamePadState.Gamepad.sThumbRY) / 32768 };

		return result;
	}
}

bool Input::PushButton(const int Button)
{
	// 押しているか
	if (m_gamePadState.Gamepad.wButtons & Button)
	{
		return true;
	}

	return false;
}

bool Input::TriggerButton(int Button)
{
	// 押しているか
	if (m_gamePadState.Gamepad.wButtons & Button && !(m_oldGamePadState.Gamepad.wButtons & Button))
	{
		return true;
	}

	return false;
}

bool Input::PullLeftTrigger()
{
	// 押しているか
	if (m_gamePadState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Input::PullRightTrigger()
{
	// 押しているか
	if (m_gamePadState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Input::SwitchLeftTrigger()
{
	// 押しているか
	if (m_gamePadState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD && !(m_oldGamePadState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Input::SwitchRightTrigger()
{
	// 押しているか
	if (m_gamePadState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD && !(m_oldGamePadState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD))
	{
		return true;
	}
	else
	{
		return false;
	}
}
