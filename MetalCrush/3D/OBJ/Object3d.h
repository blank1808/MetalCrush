#pragma once
#include "Model.h"
#include "Camera.h"
#include "LightGroup.h"
#include "CollisionInfo.h"
#include "PipelineManager.h"

#include <Windows.h>
#include <wrl.h>
#include <d3d12.h>
#include <DirectXMath.h>

#include "ParticleManager.h"

class BaseCollider;

/// <summary>
/// OBJオブジェクト
/// </summary>
class Object3d
{
public: // エイリアス
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // 定数
	static const int c_modelMaxCount = 32;

private: //静的メンバ変数
	// デバイス
	static ID3D12Device* s_dev;
	// コマンドリスト
	static ID3D12GraphicsCommandList* s_cmdList;
	//カメラクラス
	static Camera *s_camera;
	//ライトグループクラス
	static LightGroup* s_light;
	// モデルデータ
	static std::unique_ptr<Model> s_modelList[c_modelMaxCount];

public: //サブクラス
//定数バッファ用データ構造体
	struct ConstBufferData
	{
		XMMATRIX viewproj; //ビュープロジェクション行列
		XMMATRIX world; //ワールド行列
		XMFLOAT3 cameraPos; //カメラ座標(ワールド座標)
		float shininess; // 光沢度
		XMFLOAT4 color; //色(RGBA)
		XMFLOAT2 tiling; // タイリング
		XMFLOAT2 offset; // オフセット
	};

	enum ShaderType
	{
		Phong, Toon, Mono, Blend, Specular, Bloom
	};

public: //静的関数
	/// <summary>
	/// 静的初期化
	/// </summary>
	static bool StaticInitialize(ID3D12Device* device);

	/// <summary>
	/// モデルデータの読み込み
	/// </summary>
	/// <param name="modelNumber">モデルの割り振り番号</param>
	/// <param name="modelName">モデルの名前</param>
	static void LoadModel(const UINT modelNumber, const std::string& modelName, bool smoothing = false);

	/// <summary>
	/// オブジェクト生成
	/// </summary>
	/// <param name="modelNumber">モデルの割り振り番号</param>
	/// <returns>OBJオブジェクト</returns>
	static Object3d* Create(const UINT modelNumber);

	/// <summary>
	/// 描画前処理
	/// </summary>
	static void PreDraw(ID3D12GraphicsCommandList* cmdList);

	/// <summary>
	/// 描画後処理
	/// </summary>
	static void PostDraw();

public: // 静的メンバ関数
	/// <summary>
	/// モデルリストから取得
	/// </summary>
	/// <param name="modelNumber">モデルの割り振り番号</param>
	/// <returns>OBJモデル</returns>
	static Model* GetModel(const UINT modelNumber);

	/// <summary>
	/// ライトをセット
	/// </summary>
	/// <param name="lightGroup">ライト</param>
	inline static void SetLightGroup(LightGroup* light) { Object3d::s_light = light; }

private: //メンバ変数
	//定数バッファ
	ComPtr<ID3D12Resource> m_constBuff;
	//平行移動
	XMFLOAT3 m_position = { 0.0f, 0.0f, 0.0f };
	//スケール
	XMFLOAT3 m_scale = { 1.0f, 1.0f, 1.0f };
	//回転
	XMFLOAT3 m_rotation = { 0.0f, 0.0f, 0.0f };
	//色(RGBA)
	XMFLOAT4 m_color = { 1, 1, 1, 1 };
	// タイリング
	XMFLOAT2 m_tiling = { 1, 1 };
	// オフセット
	XMFLOAT2 m_offset = { 0, 0 };
	// 光沢度
	float m_shininess = 10.0f;
	//ワールド行列
	XMMATRIX m_matWorld = {};
	//モデルデータ
	Model* m_model = nullptr;
	// ビルボード
	bool m_isBillboard = false;
	// ペアレント
	Object3d* m_parent = nullptr;
	// デバック用
	const char* name = nullptr;
	//コライダー
	std::unique_ptr<BaseCollider> m_collider = nullptr;
	//ダーティーフラグ
	bool m_dirty = true;

public: //メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	Object3d() = default;

	/// <summary>
	/// デストラクタ
	/// </summary>
	virtual ~Object3d();

	/// <summary>
	/// 初期化
	/// </summary>
	virtual void Initialize();

	/// <summary>
	/// 更新
	/// </summary>
	virtual void Update();

	/// <summary>
	/// 行列の更新
	/// </summary>
	void UpdateWorldMatrix();

	/// <summary>
	/// 描画
	/// </summary>
	virtual void Draw(D3D_PRIMITIVE_TOPOLOGY primitiveType = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	/// <summary>
	/// 衝突時コールバック関数
	/// </summary>
	/// <param name="info">衝突情報</param>
	virtual void OnCollision(const CollisionInfo& info);

	/// <summary>
	/// シェーダーの変更
	/// </summary>
	/// <param name="shaderName">シェーダー名</param>
	inline void ChangeShaderPipeline(std::wstring shaderName)
	{
		m_model->ChangeShaderPipeline(shaderName);
	}

public: //アクセッサ
	/// <summary>
	/// 座標を取得
	/// </summary>
	/// <returns>座標</returns>
	inline XMFLOAT3& GetPosition() { return m_position; }

	/// <summary>
	/// 座標をセット
	/// </summary>
	/// <param name="position">座標</param>
	inline void SetPosition(const XMFLOAT3& position)
	{
		m_position = position;
		m_dirty = true;
	}

	/// <summary>
	/// 回転角を取得
	/// </summary>
	/// <returns>回転</returns>
	inline XMFLOAT3& GetRotation() { return m_rotation; }

	/// <summary>
	/// 回転角をセット
	/// </summary>
	/// <param name="rotation"></param>
	inline void SetRotation(const XMFLOAT3& rotation)
	{
		m_rotation = rotation;
		m_dirty = true;
	}

	/// <summary>
	/// スケールを取得
	/// </summary>
	/// <returns>スケール</returns>
	inline XMFLOAT3& GetScale() { return m_scale; }

	/// <summary>
	/// スケールをセット
	/// </summary>
	/// <param name="scale">スケール</param>
	inline void SetScale(const XMFLOAT3& scale)
	{
		m_scale = scale;
		m_dirty = true;
	}

	/// <summary>
	/// 色を取得
	/// </summary>
	/// <returns>色</returns>
	inline XMFLOAT4& GetColor() { return m_color; }

	/// <summary>
	/// 色をセット
	/// </summary>
	/// <param name="color">色</param>
	inline void SetColor(const XMFLOAT4& color)
	{
		m_color = color;
		m_dirty = true;
	}

	/// <summary>
	/// タイリングの取得
	/// </summary>
	/// <returns>タイリング</returns>
	inline XMFLOAT2& GetTiling() { return m_tiling; }

	/// <summary>
	/// タイリングのセット
	/// </summary>
	/// <param name="tiling">タイリング</param>
	inline void SetTiling(XMFLOAT2 tiling)
	{
		m_tiling = tiling;
	}
		
	/// <summary>
	/// オフセットの取得
	/// </summary>
	/// <returns>オフセット</returns>
	inline XMFLOAT2& GetOffset() { return m_offset; }

	/// <summary>
	/// オフセットのセット
	/// </summary>
	/// <param name="offset">オフセット</param>
	inline void SetOffset(XMFLOAT2 offset)
	{
		m_offset = offset;
	}

	/// <summary>
	/// 光沢度の取得
	/// </summary>
	/// <returns></returns>
	inline float GetShininess() { return m_shininess; }

	/// <summary>
	/// 光沢度のセット
	/// </summary>
	/// <param name="shininess">光沢度</param>
	inline void SetShininess(float shininess)
	{
		m_shininess = shininess;
	}

	/// <summary>
	/// モデルデータの取得
	/// </summary>
	/// <returns>モデルデータ</returns>
	inline Model* GetModel() { return m_model; }

	/// <summary>
	/// モデルデータセット
	/// </summary>
	inline void SetModel(Model* model)
	{
		m_model = model;
	}

	/// <summary>
	/// 正の最大座標を取得
	/// </summary>
	/// <returns>正の最大座標</returns>
	inline XMFLOAT3 GetPositivePos() { return m_model->GetPositivePos(); }

	/// <summary>
	/// 負の最小座標
	/// </summary>
	/// <returns>負の最小座標</returns>
	inline XMFLOAT3 GetNegativePos() { return m_model->GetNegativePos(); }

	/// <summary>
	/// ビルボードセット
	/// </summary>
	inline void SetBillboard(const bool isBillboard)
	{
		m_isBillboard = isBillboard;
	}

	/// <summary>
	/// ワールド行列を取得
	/// </summary>
	/// <returns>ワールド行列</returns>
	inline const XMMATRIX& GetMatWorld() { return m_matWorld; }

	/// <summary>
	/// ペアレントのワールド行列を取得
	/// </summary>
	/// <param name="parent">ペアレントのワールド行列</param>
	inline void SetParent(Object3d* parent)
	{ 
		m_parent = parent;
	}

	/// <summary>
	/// コライダーの取得
	/// </summary>
	/// <returns>コライダー</returns>
	inline BaseCollider* GetCollider() { return m_collider.get(); }

	/// <summary>
	/// コライダーのセット
	/// </summary>
	/// <param name="collider">コライダー</param>
	void SetCollider(BaseCollider* collider);
};