#pragma once
#include "Input.h"
#include "Model.h"
#include "Object3d.h"
#include "Sprite.h"
#include "Audio.h"
#include "DebugText.h"
#include "ParticleManager.h"
#include "Collision.h"
#include "Camera.h"
#include "DirectXCommon.h"
#include "BaseScene.h"
#include "LightGroup.h"
#include "FbxObject3d.h"
#include "CollisionManager.h"
#include "TouchableObject.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
#include "AutoLockOn.h"

#include <DirectXMath.h>
#include <memory>
#include <array>

/// <summary>
/// ゲームシーン
/// </summary>
class FreePlayScene : public BaseScene
{
public: // エイリアス
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // サブクラス
	// 進行度
	enum PROGRESS
	{
		CAMERA, MOVE, JUMP, DASH, N_BULLET, H_BULLET, FREE
	};

	// 壁の配置
	enum WALLNUMBER
	{
		FRONT, BACK, RIGHT, LEFT, UP, DOWN, END
	};

private: // インスタンス
	// ライト
	LightGroup* lightGroup = nullptr;

	// パーティクル
	// 爆発
	std::unique_ptr<ParticleManager> explosion = nullptr;

	// スプライト
	// レティクル
	std::unique_ptr<Sprite> Reticle = nullptr;
	// HP
	std::unique_ptr<Sprite> HPFrame = nullptr;
	std::unique_ptr<Sprite> HP = nullptr;
	// ブーストゲージ
	std::unique_ptr<Sprite> BoosterFrame = nullptr;
	std::unique_ptr<Sprite> Booster = nullptr;
	// 残弾
	std::unique_ptr<Sprite> NormalBulletNum[2];
	std::unique_ptr<Sprite> HomingBulletNum[2];
	std::unique_ptr<Sprite> BulletNumFrame[2] = {};
	// レーダー
	std::unique_ptr<Sprite> Rader = nullptr;
	std::unique_ptr<Sprite> RaderFrame = nullptr;
	std::unique_ptr<Sprite> enemyMarkers;
	// オーダー
	std::unique_ptr<Sprite> order = nullptr;

	// OBJ
	// 砂漠
	std::unique_ptr<TouchableObject> m_stage = nullptr;
	// 敵
	std::unique_ptr<Enemy> m_enemy = nullptr;
	// プレイヤー
	std::unique_ptr<Player> m_player = nullptr;

	// その他
	// ロックオンリスト
	std::unique_ptr<AutoLockOn> m_lockList = nullptr;
	// コリジョンマネージャ
	CollisionManager* m_collisionManager = nullptr;

private: // メンバ変数
	// レーダのアニメーションフレーム
	int m_raderAnimation = 0;
	// レーダーの範囲
	float m_raderRange = 0;
	// 終了フラグ
	bool m_isEnd = false;
	// 進行フラグ
	int m_isProgress = 0;

public: // メンバ関数
	/// <summary>
	/// デストラクタ
	/// </summary>
	~FreePlayScene() override;

	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// 更新
	/// </summary>
	void Update() override;

	/// <summary>
	/// 描画
	/// </summary>
	void Draw() override;

	/// <summary>
	/// スプライト描画
	/// </summary>
	void DrawSprite() override;

	/// <summary>
	/// 変数初期化
	/// </summary>
	void InitializeParameter();

	/// <summary>
	/// 描画
	/// </summary>
	void DrawObjects(ID3D12GraphicsCommandList* cmdList);

public: // メンバ関数
	/// <summary>
	/// 進行の更新
	/// </summary>
	void UpdateProgress();

	/// <summary>
	/// HUD更新
	/// </summary>
	void UpdateHUD();

	/// <summary>
	/// シーン遷移
	/// </summary>
	void ChangeScene();

	/// <summary>
	/// 衝突判定
	/// </summary>
	void CheckAllCollisions();

	/// <summary>
	/// プレイヤーとエネミーの衝突判定	
	/// </summary>
	void CheckPlayer2Enemy();

	/// <summary>
	/// プレイヤーの弾とエネミーの衝突判定
	/// </summary>
	void CheckPlayerBullets2Enemy();
};
