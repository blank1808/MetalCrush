#pragma once
#include <Windows.h>
#include <DirectXMath.h>
#include "Input.h"
#include "Object3d.h"
#include "FbxObject3d.h"
#include "Sprite.h"
#include "Audio.h"
#include "DebugText.h"
#include "ParticleManager.h"
#include "Collision.h"
#include "Camera.h"
#include "DirectXCommon.h"
#include "BaseScene.h"
#include <memory>

/// <summary>
/// エンドシーン
/// </summary>
class EndScene : public BaseScene
{
public: // エイリアス
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: //インスタンス
	//スプライト
	std::unique_ptr<Sprite> endScreen = nullptr;
	std::unique_ptr<Sprite> clearScreen = nullptr;

private: // メンバ変数
	// リザルトスコア
	int m_resultScore = 0;
	// どう終わったか
	std::string m_end;
	// 画面切り替え
	bool m_isChange = false;

public: //メンバ関数
	/// <summary>
	/// デストラクタ
	/// </summary>
	~EndScene() override;

	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// 更新
	/// </summary>
	void Update() override;

	/// <summary>
	/// 描画
	/// </summary>
	void Draw() override;

	/// <summary>
	/// スプライト描画
	/// </summary>
	void DrawSprite() override;

	/// <summary>
	/// 変数初期化
	/// </summary>
	void InitializeParameter();

	/// <summary>
	/// 描画
	/// </summary>
	void DrawObject(ID3D12GraphicsCommandList* cmdList);

	/// <summary>
	/// テキスト読み込み
	/// </summary>
	void LoadText();
};