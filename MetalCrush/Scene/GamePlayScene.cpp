#include "GamePlayScene.h"
#include "SceneManager.h"
#include "SceneChange.h"

#include <fstream>
#include <Helper.h>
#include <SafeDelete.h>
#include <cassert>
#include <fbxsdk.h>
#include <Random.h>

using namespace DirectX;

GamePlayScene::~GamePlayScene()
{
	
}

void GamePlayScene::Initialize()
{
	// ライト生成
	lightGroup = LightGroup::GetInstance();

	// パーティクル
	explosion.reset(ParticleManager::Create("Particle/FireParticle.png"));
	sandStorm.reset(ParticleManager::Create("Particle/effect2.png"));

	// OBJ
	m_desert.reset(TouchableObject::Create(0, 10, 10));
	m_skyWall.reset(Object3d::Create(1));
	m_sun.reset(Object3d::Create(3));
	for (int i = 0; i < 7; i++)
	{
		m_enemy.emplace_back(new Enemy);
	}
	m_player.reset(new Player);

	// スプライト
	Reticle.reset(Sprite::Create(1, { 0, 0 }, { 0.5f, 0.5f }));
	HPFrame.reset(Sprite::Create(3, { 0, 0 }, { 0.5f, 1.0f }));
	HP.reset(Sprite::Create(4, { 0, 0 }, { 0, 0.5f }));
	BoosterFrame.reset(Sprite::Create(5, { 0, 0 }, { 0.5f, 1.0f }));
	Booster.reset(Sprite::Create(6, { 0, 0 }, { 0, 0.5f }));
	BulletNumFrame[0].reset(Sprite::Create(8, { 0, 0 }, { 0.5f, 1.0f }));
	BulletNumFrame[1].reset(Sprite::Create(23, { 0, 0 }, { 0.5f, 1.0f }));
	for (int i = 0; i < 2; i++)
	{
		NormalBulletNum[i].reset(Sprite::Create(2, { 0.5f, 0.5f }, { 0.5f, 1.0f }));
		HomingBulletNum[i].reset(Sprite::Create(2, { 0.5f, 0.5f }, { 0.5f, 1.0f }));
	}
	Rader.reset(Sprite::Create(9, { 0, 0 }, { 0.0f, 0.5f }));
	RaderFrame.reset(Sprite::Create(14, { 0, 0 }, { 0.0f, 0.5f }));
	for (int i = 0; i < m_enemy.size(); i++)
	{
		enemysMarker.emplace_back(Sprite::Create(10, { 0, 0 }, { 0.5f, 0.5f }));
	}
	gameOver.reset(Sprite::Create(7));
	order.reset(Sprite::Create(16, { 0, 0 }, { 0.5f, 0.0f }));

	// その他
	m_lockList.reset(new AutoLockOn);
	m_collisionManager = CollisionManager::GetInstance();

	// オーディオ
	//audio->Initialize();

	// 変数の初期化
	InitializeParameter();

	// 乱数初期化
	srand((unsigned int)time(NULL));
}

void GamePlayScene::InitializeParameter()
{
	// 砂漠
	m_desert->SetScale({ 1, 1, 1 });
	m_desert->Update();

	// 壁
	m_skyWall->ChangeShaderPipeline(L"Texture");
	m_skyWall->Update();

	// 太陽
	m_sun->ChangeShaderPipeline(L"Texture");
	float distance = 10000;
	m_sun->SetPosition({ distance, distance, 0 });
	m_sun->SetScale({ distance / 25, distance / 25, distance / 25 });
	m_sun->Update();

	// ライト
	lightGroup->SetDirLightDir({ -1, -1, 0, 1 });
	lightGroup->SetDirLightColor({ 1.0f, 1.0f, 1.0f });
	lightGroup->Update();

	// スプライト
	XMFLOAT4 spriteColor = { 1, 1, 1, 0.8f };
	HPFrame->SetPosition({ WinApp::window_width / 2, 72 * 9 });
	HP->SetPosition({ WinApp::window_width / 2 - 248, 72 * 9 - 36 });
	HP->SetColor(spriteColor);
	BoosterFrame->SetPosition({ WinApp::window_width / 2, 72 * 10 });
	Booster->SetPosition({ WinApp::window_width / 2 - 236, 72 * 10 - 36 });
	Booster->SetColor(spriteColor);
	BulletNumFrame[0]->SetPosition({ 128.0f * 9 - 7, 72.0f * 10 - 7 });
	BulletNumFrame[1]->SetPosition({ 128.0f * 9 - 7, 72.0f * 8 - 7 });
	for (int i = 0; i < 2; i++)
	{
		NormalBulletNum[i]->SetPosition({ 128.0f * 8.5f + 40.0f * i, 72.0f * 10 - 12 });
		NormalBulletNum[i]->SetTexSize({ 64, 72 });
		NormalBulletNum[i]->SetSize({ 64, 72 });
		NormalBulletNum[i]->SetColor(spriteColor);
		HomingBulletNum[i]->SetPosition({ 128.0f * 9.0f + 40.0f * i, 72.0f * 8 - 12 });
		HomingBulletNum[i]->SetTexSize({ 64, 72 });
		HomingBulletNum[i]->SetSize({ 64, 72 });
		HomingBulletNum[i]->SetColor(spriteColor);
	}
	Rader->SetPosition({ 0, 600 });
	Rader->SetTexSize({ 240, 240 });
	Rader->SetSize({ 240, 240 });
	Rader->SetColor(spriteColor);
	RaderFrame->SetPosition({ 0, 600 });
	order->SetPosition({ WinApp::window_width / 2, 0 });

	// カメラ
	Camera::GetInstance()->SetCameraAngles();
	Camera::GetInstance()->SetTarget({ 0, 0, 0 });
	Camera::GetInstance()->SetEyeDistance({0, 0, -5});
	Camera::GetInstance()->UpdateMatView();

	// 変数
	m_raderAnimation = 0;
	m_raderRange = 1000;
	m_isClear = false;
	m_isGameOver = false;

	// シーン切り替え
	SceneChange::GetInstance()->SetColor({ 0, 0, 0, 1 });
}

void GamePlayScene::Update()
{
	// シーン切り替え
	if (m_isClear == false && m_isGameOver == false)
	{
		SceneChange::GetInstance()->IsBrighten(0.1f);
	}

	// プレイヤー
	m_player->Update();

	// エネミー
	for (const auto& m : m_enemy)
	{
		m->Update();
	}

	// オートロック切り替え
	if (Input::GetInstance()->TriggerButton(BUTTON::D_CROSS))
	{
		m_lockList->ChangeTargetLock();
	}

	// 衝突判定
	CheckAllCollisions();

	// HUD
	UpdateHUD();

	// 砂煙
	SandStormUpdate();

	// シーン遷移
	GameOver();

	// ライト更新
	lightGroup->Update();
	
	// カメラ更新
	Camera::GetInstance()->UpdateMatView();
}

void GamePlayScene::Draw()
{
	// コマンドリストの取得
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	// 各描画
	DrawObjects(cmdList);
}

void GamePlayScene::DrawSprite()
{
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	// HUD描画
	Sprite::PreDraw(cmdList);

	HPFrame->Draw();
	HP->Draw();
	BoosterFrame->Draw();
	Booster->Draw();
	BulletNumFrame[0]->Draw();
	BulletNumFrame[1]->Draw();
	for (const auto& m : NormalBulletNum)
	{
		m->Draw();
	}
	for (const auto& m : HomingBulletNum)
	{
		m->Draw();
	}
	Reticle->Draw();
	Rader->Draw();
	for (const auto& m : enemysMarker)
	{
		m->Draw();
	}
	RaderFrame->Draw();
	order->Draw();

	SceneChange::GetInstance()->Draw();

	Sprite::PostDraw();
}

void GamePlayScene::DrawObjects(ID3D12GraphicsCommandList* cmdList)
{
	// OBJオブジェクト描画
	Object3d::PreDraw(cmdList);

	// 地面
	m_desert->Draw();
	// 壁
	m_skyWall->Draw();
	// 太陽
	m_sun->Draw();

	Object3d::PostDraw();

	// プレイヤー
	m_player->Draw(cmdList);

	// エネミー
	for (const auto& m : m_enemy)
	{
		m->Draw(cmdList);
	}

	// パーティクル描画
	ParticleManager::PreDraw(cmdList);

	explosion->Draw();
	m_player->DrawEffect();
	for (const auto& m : m_enemy)
	{
		m->DrawEffect();
	}
	sandStorm->Draw();

	ParticleManager::PostDraw();
}

void GamePlayScene::UpdateHUD()
{
	// ゲージ
	HP->SetSize({ 496.0f * (float)m_player->GetPlayerHP() / 20, 56 });
	Booster->SetSize({ 472.0f * (float)m_player->GetBoosterGauge() / 360, 28 });

	// 弾数表記
	int tmp = m_player->GetNormalBulletNum();
	NormalBulletNum[0]->SetLeftTop({ static_cast<float>(tmp / 10) * 64, 0 });
	tmp %= 10;
	NormalBulletNum[1]->SetLeftTop({ static_cast<float>(tmp) * 64, 0 });

	tmp = m_player->GetHomingBulletNum();
	HomingBulletNum[0]->SetLeftTop({ static_cast<float>(tmp / 10) * 64, 0 });
	tmp %= 10;
	HomingBulletNum[1]->SetLeftTop({ static_cast<float>(tmp) * 64, 0 });

	// レーダー
	m_raderAnimation++;
	Rader->SetLeftTop({ static_cast<float>(m_raderAnimation / 3) * 240, 0 });
	if (19 * 3 < m_raderAnimation)
	{
		Rader->SetLeftTop({ 0, 0 });
		if (40 * 3 < m_raderAnimation)
		{
			m_raderAnimation = 0;
		}
	}
	// レーダーマーカー更新
	if (m_raderAnimation / 3 == 1)
	{
		for (int i = 0; i < m_enemy.size(); i++)
		{
			XMFLOAT3 offset = Helper::VectorFloat3(m_enemy[i]->GetPosition(), m_player->GetPosition());
			offset.z *= -1;
			offset = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, offset);
			if (fabs(offset.x) < m_raderRange * 0.95f && fabs(offset.z) < m_raderRange * 0.95f && m_enemy[i]->GetAlive() == true)
			{
				enemysMarker[i]->SetInvisible(false);
				XMFLOAT2 pos = Rader->GetPostion();
				pos.x += 120;
				enemysMarker[i]->SetPosition({ pos.x + 120.0f * offset.x / m_raderRange, pos.y + 120.0f * offset.z / m_raderRange });
			}
			else
			{
				enemysMarker[i]->SetInvisible(true);
			}
		}
	}
}

void GamePlayScene::SandStormUpdate()
{
	XMFLOAT3 start = Camera::GetInstance()->GetTarget();
	XMFLOAT3 pos = { start.x - 200, 25, m_desert->GetPositivePos().z};
	XMFLOAT3 neg = { start.x - 200, 0, m_desert->GetNegativePos().z };
	start.x = -540;
	start.y = (float)(rand() % (int)(pos.y - neg.y) + neg.y);
	start.z = (float)(rand() % (int)(pos.z - neg.z) + neg.z);
	XMFLOAT4 startColor = { 0.275f, 0.25f, 0.2f, 1.0f };
	sandStorm->Add(220, start, { 5, 0, 0 }, { 0, 0, 0 }, startColor, { 0.0f, 0.0f, 0.0f, 0.0f }, 40.0f, 40.0f, false);
}

void GamePlayScene::GameOver()
{
	m_isClear = true;
	for (const auto& m : m_enemy)
	{
		if (m->GetAlive() == true)
		{
			m_isClear = false;
			break;
		}
	}
	if (m_player->GetPlayerHP() == 0)
	{
		m_isGameOver = true;
	}

	if (m_isClear == true || m_isGameOver == true)
	{
		if (SceneChange::GetInstance()->IsBlockOut(0.1f))
		{
			ComputeScore();
			SceneManager::GetInstance()->ChangeScene("END");
		}
	}
}

void GamePlayScene::ComputeScore()
{
	m_resultScore = 1000;
	writeText();
}

void GamePlayScene::writeText()
{
	std::string score = std::to_string(m_resultScore);
	std::string end;
	if (m_isClear == true)
	{
		end = "CLEAR";
	}
	else if (m_isGameOver == true)
	{
		end = "GAMEOVER";
	}

	std::ofstream ofs("Resources/ScoreList.txt");
	if (!ofs)
	{
		assert(0);
	}
	else
	{
		ofs << " " << end << " " << score;
	}
	ofs.close();
}

void GamePlayScene::CheckAllCollisions()
{
#pragma region プレイヤーとエネミーの衝突判定

	CheckPlayer2Enemy();

#pragma endregion

#pragma region プレイヤー弾とエネミーの衝突判定

	CheckPlayerBullets2Enemy();

#pragma endregion

#pragma region プレイヤーとエネミー弾の衝突判定

	CheckPlayer2EnemyBullets();

#pragma endregion
}

void GamePlayScene::CheckPlayer2Enemy()
{
	// 射程内にいるものを探す
	for (auto& m : m_enemy)
	{
		if (m->GetAlive() == true)
		{
			// 射程内か調べる
			if (m_player->SearchTarget(m->GetPosition()) && Camera::GetInstance()->ObjectComeInSight(m->GetPosition()))
			{
				m_lockList->LockOn(m.get());
			}
			else
			{
				m_lockList->Lost(m.get());
			}
			if (m->SearchTarget(m_player->GetPosition()))
			{
				// エネミーの発射
				m->ShotBullet(m_player->GetPosition());
			}
		}
	}

	// 更新
	m_lockList->Update();

	// 何もロックしてないなら
	if (m_lockList->GetTargetNum() == 0)
	{
		// まっすぐ飛ぶ
		Reticle->SetPosition({ 0, 0 });
		Reticle->SetInvisible(true);

		m_player->SetTarget(false);
	}
	else
	{
		// ロック先に打つ
		Reticle->SetPosition(Camera::GetInstance()->Convert3DPosTo2DPos(m_lockList->GetTargetEnemy()->GetPosition()));
		Reticle->SetInvisible(false);

		m_player->SetTarget(true, m_lockList->GetTargetEnemy()->GetPosition());
	}
}

void GamePlayScene::CheckPlayerBullets2Enemy()
{
	const std::list<Bullet*>& playerBullets = m_player->GetPlayerBullets();
	for (const auto& pB : playerBullets)
	{
		if (pB->GetAlive() == true)
		{
			for (auto& e : m_enemy)
			{
				if (e->GetAlive() == true && Helper::LengthFloat3(pB->GetPosition(), e->GetPosition()) < 4.0f)
				{
					pB->SetAlive(false);
					if (e->EnemyDamage(1) == true)
					{
						e->SetAlive(false);
						m_lockList->Lost(e.get());
						int num = Random::GetRanNum(5, 7);
						for (int i = 0; i < num; i++)
						{
							XMFLOAT3 vel = {};
							vel.x += static_cast<float>(Random::GetRanNum(0, 4) - 2) / 10;
							vel.y += static_cast<float>(Random::GetRanNum(0, 2)) / 10;
							vel.z += static_cast<float>(Random::GetRanNum(0, 4) - 2) / 10;
							explosion->Add(60, e->GetPosition(), vel, { 0, 0, 0 }, { 1.0f, 0.3f, 0.1f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 50.0f, false);
						}
					}
					else
					{
						for (int i = 0; i < 4; i++)
						{
							XMFLOAT3 pos = e->GetPosition();
							pos.x += Random::GetRanNum(0, 10) - 5;
							pos.y += Random::GetRanNum(0, 4) - 1;
							pos.z += Random::GetRanNum(0, 10) - 5;
							for (int j = 0; j < 3; j++)
							{
								explosion->Add(60, pos, { 0, 0, 0 }, { 0, 0, 0 }, { 1.0f, 0.3f, 0.1f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 10.0f, false);
							}
						}
					}
				}
				else if (pB->CheckCollision() == true && pB->GetPosition().y < m_desert->GetPositivePos().y)
				{
					pB->SetAlive(false);

					for (int i = 0; i < 3; i++)
					{
						XMFLOAT3 vel = {};
						vel.y += static_cast<float>(Random::GetRanNum(1, 2)) / 10;
						sandStorm->Add(60, pB->GetPosition(), vel, { 0, 0, 0 }, { 0.275f, 0.25f, 0.2f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 40.0f, false);
					}
				}
			}
		}
	}
}

void GamePlayScene::CheckPlayer2EnemyBullets()
{
	for (auto& e : m_enemy)
	{
		const std::list<Bullet*>& enemyrBullets = e->GetEnemyBullet();

		for (const auto& eB : enemyrBullets)
		{
			if (Helper::LengthFloat3(eB->GetPosition(), m_player->GetPosition()) < 4.0f && eB->GetAlive() == true)
			{
				eB->SetAlive(false);
				m_player->PlayerDamage(2);

				if (m_player->GetPlayerHP() <= 0)
				{
					int num = Random::GetRanNum(5, 7);
					for (int i = 0; i < num; i++)
					{
						XMFLOAT3 vel = {};
						vel.x += static_cast<float>(Random::GetRanNum(0, 4) - 2) / 10;
						vel.y += static_cast<float>(Random::GetRanNum(0, 2)) / 10;
						vel.z += static_cast<float>(Random::GetRanNum(0, 4) - 2) / 10;
						explosion->Add(60, m_player->GetPosition(), vel, { 0, 0, 0 }, { 1.0f, 0.3f, 0.1f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 30.0f, false);
					}
				}
				else
				{
					for (int i = 0; i < 4; i++)
					{
						XMFLOAT3 pos = m_player->GetPosition();
						pos.x += Random::GetRanNum(0, 2) - 1;
						pos.y += Random::GetRanNum(0, 2) - 1;
						pos.z += Random::GetRanNum(0, 2) - 1;
						for (int j = 0; j < 2; j++)
						{
							explosion->Add(60, pos, { 0, 0, 0 }, { 0, 0, 0 }, { 1.0f, 0.3f, 0.1f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 2.0f, false);
						}
					}
				}
			}
			else if (eB->CheckCollision() == true && eB->GetPosition().y < m_desert->GetPositivePos().y)
			{
				eB->SetAlive(false);

				for (int i = 0; i < 3; i++)
				{
					XMFLOAT3 vel = {};
					vel.y += static_cast<float>(Random::GetRanNum(1, 2)) / 10;
					sandStorm->Add(60, eB->GetPosition(), vel, { 0, 0, 0 }, { 0.275f, 0.25f, 0.2f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 40.0f, false);
				}
			}
		}
	}
}
