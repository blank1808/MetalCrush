#include "TitleScene.h"
#include "SceneManager.h"
#include "SceneChange.h"

#include <Random.h>

using namespace DirectX;
using namespace Microsoft::WRL;

TitleScene::~TitleScene()
{

}

void TitleScene::Initialize()
{
	// パーティクル
	sandStorm.reset(ParticleManager::Create("Particle/effect2.png"));

	//スプライト
	m_startScreen.reset(Sprite::Create(11));

	//オブジェクト
	m_desert.reset(Object3d::Create(0));
	m_skyWall.reset(Object3d::Create(1));

	//パラメーター
	InitializeParameter();

	//オーディオ

}

void TitleScene::InitializeParameter()
{
	// 砂漠
	m_desert->SetScale({ 1, 1, 1 });
	m_desert->Update();

	// 壁
	m_skyWall->ChangeShaderPipeline(L"Texture");
	m_skyWall->Update();

	// カメラ
	Camera::GetInstance()->SetCameraAngles();
	Camera::GetInstance()->SetTarget({0, 5, 0});
	Camera::GetInstance()->SetEyeDistance({ 0, 0, -5 });
	Camera::GetInstance()->UpdateMatView();

	// ライト
	LightGroup::GetInstance()->SetDirLightDir({ -1, -1, 0, 1 });
	LightGroup::GetInstance()->SetDirLightColor({ 1.0f, 1.0f, 1.0f });
	LightGroup::GetInstance()->Update();

	// 変数
	m_changeScene = false;

	// 切り替え
	SceneChange::GetInstance()->SetColor({ 0, 0, 0, 1 });
}

void TitleScene::Update()
{
	// 砂煙
	XMFLOAT3 start = Camera::GetInstance()->GetTarget();
	XMFLOAT3 pos = { start.x - 200, 25, m_desert->GetPositivePos().z };
	XMFLOAT3 neg = { start.x - 200, 0, m_desert->GetNegativePos().z };
	start.x = -540;
	start.y = (float)(rand() % (int)(pos.y - neg.y) + neg.y);
	start.z = (float)(rand() % (int)(pos.z - neg.z) + neg.z);
	XMFLOAT4 startColor = { 0.275f, 0.25f, 0.2f, 1.0f };
	sandStorm->Add(220, start, { 5, 0, 0 }, { 0, 0, 0 }, startColor, { 0.0f, 0.0f, 0.0f, 0.0f }, 40.0f, 40.0f, false);

	if (m_changeScene == false)
	{
		if (SceneChange::GetInstance()->IsBrighten(0.1f))
		{
			// 次のシーンに移行
			if (Input::GetInstance()->PushButton(BUTTON::B))
			{
				m_changeScene = true;
			}
		}
	}
	else
	{
		if (SceneChange::GetInstance()->IsBlockOut(0.1f))
		{
			SceneManager::GetInstance()->ChangeScene("FREE");
		}
	}
}

void TitleScene::Draw()
{
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	//各描画
	DrawObjects(cmdList);
}

void TitleScene::DrawSprite()
{
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	//スプライト描画
	Sprite::PreDraw(cmdList);

	m_startScreen->Draw();
	SceneChange::GetInstance()->Draw();

	Sprite::PostDraw();
}

void TitleScene::DrawObjects(ID3D12GraphicsCommandList* cmdList)
{
	//オブジェクト描画
	Object3d::PreDraw(cmdList);

	m_desert->Draw();
	m_skyWall->Draw();

	Object3d::PostDraw();

	// パーティクル
	ParticleManager::PreDraw(cmdList);

	sandStorm->Draw();

	ParticleManager::PostDraw();
}
