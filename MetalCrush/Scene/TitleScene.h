#pragma once
#include <Windows.h>
#include <DirectXMath.h>
#include <memory>

#include "BaseScene.h"
#include "DirectXCommon.h"
#include "Input.h"
#include "Audio.h"
#include "Camera.h"
#include "Sprite.h"
#include "Object3d.h"

/// <summary>
/// タイトルシーン
/// </summary>
class TitleScene : public BaseScene
{
public: // エイリアス
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // インスタンス
	// パーティクル
	std::unique_ptr<ParticleManager> sandStorm = nullptr;

	// スプライト
	std::unique_ptr<Sprite> m_startScreen = nullptr;

	// OBJ
	std::unique_ptr<Object3d> m_desert = nullptr;
	std::unique_ptr<Object3d> m_skyWall = nullptr;

private: // メンバ変数
	// シーンを変える
	bool m_changeScene = false;

public: // メンバ関数
	/// <summary>
	/// デストラクタ
	/// </summary>
	~TitleScene() override;

	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// 更新
	/// </summary>
	void Update() override;

	/// <summary>
	/// 描画
	/// </summary>
	void Draw() override;

	/// <summary>
	/// スプライト描画
	/// </summary>
	void DrawSprite() override;

	/// <summary>
	/// 変数初期化
	/// </summary>
	void InitializeParameter();

	/// <summary>
	/// オブジェクト描画
	/// </summary>
	/// <param name="cmdList">コマンドリスト</param>
	void DrawObjects(ID3D12GraphicsCommandList* cmdList);
};