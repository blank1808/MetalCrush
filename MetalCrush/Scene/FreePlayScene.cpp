#include "FreePlayScene.h"
#include "SceneManager.h"
#include "SceneChange.h"

#include <Helper.h>
#include <SafeDelete.h>
#include <cassert>
#include <fbxsdk.h>
#include <random>
#include <Random.h>

using namespace DirectX;

FreePlayScene::~FreePlayScene()
{

}

void FreePlayScene::Initialize()
{
	// ライト生成
	lightGroup = LightGroup::GetInstance();

	// パーティクル
	explosion.reset(ParticleManager::Create("Particle/FireParticle.png"));

	// OBJ
	m_stage.reset(TouchableObject::Create(7));
	m_enemy.reset(new Enemy(6));
	m_player.reset(new Player);

	// スプライト
	Reticle.reset(Sprite::Create(1, { 0, 0 }, { 0.5f, 0.5f }));
	HPFrame.reset(Sprite::Create(3, { 0, 0 }, { 0.5f, 1.0f }));
	HP.reset(Sprite::Create(4, { 0, 0 }, { 0, 0.5f }));
	BoosterFrame.reset(Sprite::Create(5, { 0, 0 }, { 0.5f, 1.0f }));
	Booster.reset(Sprite::Create(6, { 0, 0 }, { 0, 0.5f }));
	BulletNumFrame[0].reset(Sprite::Create(8, { 0, 0 }, { 0.5f, 1.0f }));
	BulletNumFrame[1].reset(Sprite::Create(23, { 0, 0 }, { 0.5f, 1.0f }));
	for (int i = 0; i < 2; i++)
	{
		NormalBulletNum[i].reset(Sprite::Create(2, { 0.5f, 0.5f }, { 0.5f, 1.0f }));
		HomingBulletNum[i].reset(Sprite::Create(2, { 0.5f, 0.5f }, { 0.5f, 1.0f }));
	}
	Rader.reset(Sprite::Create(9, { 0, 0 }, { 0.0f, 0.5f }));
	RaderFrame.reset(Sprite::Create(14, { 0, 0 }, { 0.0f, 0.5f }));
	enemyMarkers.reset(Sprite::Create(10, { 0, 0 }, { 0.5f, 0.5f }));
	order.reset(Sprite::Create(13, { 0, 0 }, { 0.5f, 0.0f }));

	// その他
	m_lockList.reset(new AutoLockOn);
	m_collisionManager = CollisionManager::GetInstance();

	// オーディオ
	//audio->Initialize();

	// 変数の初期化
	InitializeParameter();

	// 乱数初期化
	srand((unsigned int)time(NULL));
}

void FreePlayScene::InitializeParameter()
{
	// 敵
	m_enemy->SetPosition({ 0, 0, 0 });
	m_enemy->SetVelocity({ 0, 0, 0 });

	// 砂漠
	m_stage->SetScale({ 1, 1, 1 });
	m_stage->Update();

	// ライト
	lightGroup->SetDirLightActive(true);
	lightGroup->SetDirLightDir({ -1, -1, 0, 1 });
	lightGroup->SetDirLightColor({ 1.0f, 1.0f, 1.0f });

	// スプライト
	XMFLOAT4 spriteColor = { 1, 1, 1, 0.8f };
	HPFrame->SetPosition({ WinApp::window_width / 2, 72 * 9 });
	HP->SetPosition({ WinApp::window_width / 2 - 248, 72 * 9 - 36 });
	HP->SetColor(spriteColor);
	BoosterFrame->SetPosition({ WinApp::window_width / 2, 72 * 10 });
	Booster->SetPosition({ WinApp::window_width / 2 - 236, 72 * 10 - 36 });
	Booster->SetColor(spriteColor);
	BulletNumFrame[0]->SetPosition({128.0f * 9 - 7, 72.0f * 10 - 7});
	BulletNumFrame[1]->SetPosition({128.0f * 9 - 7, 72.0f * 8 - 7});
	for (int i = 0; i < 2; i++)
	{
		NormalBulletNum[i]->SetPosition({ 128.0f * 8.5f + 40.0f * i, 72.0f * 10 - 12 });
		NormalBulletNum[i]->SetTexSize({ 64, 72 });
		NormalBulletNum[i]->SetSize({ 64, 72 });
		NormalBulletNum[i]->SetColor(spriteColor);
		HomingBulletNum[i]->SetPosition({ 128.0f * 9.0f + 40.0f * i, 72.0f * 8 - 12 });
		HomingBulletNum[i]->SetTexSize({ 64, 72 });
		HomingBulletNum[i]->SetSize({ 64, 72 });
		HomingBulletNum[i]->SetColor(spriteColor);
	}
	Rader->SetPosition({ 0, 600 });
	Rader->SetTexSize({ 240, 240 });
	Rader->SetSize({ 240, 240 });
	Rader->SetColor(spriteColor);
	RaderFrame->SetPosition({ 0, 600 });
	order->SetPosition({ WinApp::window_width / 2, 0 });

	// カメラ
	Camera::GetInstance()->SetCameraAngles();
	Camera::GetInstance()->SetTarget({ 0, 0, 0 });
	Camera::GetInstance()->SetEyeDistance({ 0, 0, -5 });
	Camera::GetInstance()->UpdateMatView();

	// 変数
	m_raderAnimation = 0;
	m_raderRange = 1000;
	m_isEnd = false;
	m_isProgress = CAMERA;

	// シーン切り替え
	SceneChange::GetInstance()->SetColor({ 0, 0, 0, 1 });
}

void FreePlayScene::Update()
{
#pragma region ゲームメインシステム

	// シーン切り替え
	if (m_isEnd == false)
	{
		SceneChange::GetInstance()->IsBrighten(0.1f);
	}

	// プレイヤー
	m_player->Update();

	// エネミー
	m_enemy->Update();

	// ロック切り替え
	if (Input::GetInstance()->TriggerButton(BUTTON::U_CROSS))
	{
		m_lockList->ChangeTargetLock();
	}

	// 衝突判定
	CheckAllCollisions();

	//進行推移の更新
	UpdateProgress();

	// HUD
	UpdateHUD();

	// シーン遷移
	ChangeScene();

#pragma endregion

#pragma region カメラとライトの更新

	// ライト更新
	lightGroup->Update();

	// カメラ更新
	Camera::GetInstance()->UpdateMatView();

#pragma endregion
}

void FreePlayScene::Draw()
{
	// コマンドリストの取得
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	// 各描画
	DrawObjects(cmdList);
}

void FreePlayScene::DrawSprite()
{
	// コマンドリストの取得
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	// スプライト描画
	Sprite::PreDraw(cmdList);

	HPFrame->Draw();
	HP->Draw();
	BoosterFrame->Draw();
	Booster->Draw();
	BulletNumFrame[0]->Draw();
	BulletNumFrame[1]->Draw();
	for (const auto& m : NormalBulletNum)
	{
		m->Draw();
	}
	for (const auto& m : HomingBulletNum)
	{
		m->Draw();
	}
	Reticle->Draw();
	Rader->Draw();
	enemyMarkers->Draw();
	RaderFrame->Draw();
	order->Draw();

	SceneChange::GetInstance()->Draw();

	Sprite::PostDraw();
}

void FreePlayScene::DrawObjects(ID3D12GraphicsCommandList* cmdList)
{
	// OBJオブジェクト描画
	Object3d::PreDraw(cmdList);

	// 地面
	m_stage->Draw();

	Object3d::PostDraw();

	// プレイヤー
	m_player->Draw(cmdList);

	// エネミー
	m_enemy->Draw(cmdList);

	// パーティクル描画
	ParticleManager::PreDraw(cmdList);

	m_player->DrawEffect();
	explosion->Draw();

	ParticleManager::PostDraw();
}

void FreePlayScene::UpdateProgress()
{
	if (m_isProgress == CAMERA)
	{
		if (1.0f <= Helper::LengthFloat2(Input::GetInstance()->RightStickAngle()))
		{
			m_isProgress = MOVE;
			order->TextureChange(18);
		}
	}
	else if (m_isProgress == MOVE)
	{
		if (1.0f <= Helper::LengthFloat2(Input::GetInstance()->LeftStickAngle()))
		{
			m_isProgress = JUMP;
			order->TextureChange(19);
		}
	}
	else if (m_isProgress == JUMP)
	{
		if (Input::GetInstance()->SwitchLeftTrigger() == true)
		{
			m_isProgress = DASH;
			order->TextureChange(20);
		}
	}
	else if (m_isProgress == DASH)
	{
		if (Input::GetInstance()->SwitchRightTrigger() == true)
		{
			m_isProgress = N_BULLET;
			order->TextureChange(21);
		}
	}
	else if (m_isProgress == N_BULLET)
	{
		if (Input::GetInstance()->PushButton(BUTTON::RB) == true)
		{
			m_isProgress = H_BULLET;
			order->TextureChange(22);
		}
	}
	else if (m_isProgress == H_BULLET)
	{
		if (Input::GetInstance()->PushButton(BUTTON::LB) == true)
		{
			m_isProgress = FREE;
			order->TextureChange(17);
		}
	}
}

void FreePlayScene::UpdateHUD()
{
	// ゲージ
	HP->SetSize({ 496.0f * (float)m_player->GetPlayerHP() / 20, 56 });
	Booster->SetSize({ 472.0f * (float)m_player->GetBoosterGauge() / 360, 28 });

	// 弾数表記
	int tmp = m_player->GetNormalBulletNum();
	NormalBulletNum[0]->SetLeftTop({ static_cast<float>(tmp / 10) * 64, 0 });
	tmp %= 10;
	NormalBulletNum[1]->SetLeftTop({ static_cast<float>(tmp) * 64, 0 });

	tmp = m_player->GetHomingBulletNum();
	HomingBulletNum[0]->SetLeftTop({ static_cast<float>(tmp / 10) * 64, 0 });
	tmp %= 10;
	HomingBulletNum[1]->SetLeftTop({ static_cast<float>(tmp) * 64, 0 });


	// レーダーアニメーション
	m_raderAnimation++;
	// 3フレーム更新
	Rader->SetLeftTop({ static_cast<float>(m_raderAnimation / 3) * 240, 0 });
	if (19 * 3 < m_raderAnimation)
	{
		Rader->SetLeftTop({ 0, 0 });
		if (40 * 3 < m_raderAnimation)
		{
			m_raderAnimation = 0;
		}
	}

	// レーダーマーカー更新
	if (m_raderAnimation / 3 == 1)
	{
		XMFLOAT3 offset = Helper::VectorFloat3(m_enemy->GetPosition(), m_player->GetPosition());
		offset.z *= -1;
		offset = Camera::GetInstance()->ConvertWindowYPos({ 0, 0, 0 }, offset);
		if (fabs(offset.x) < m_raderRange * 0.95f && fabs(offset.z) < m_raderRange * 0.95f && m_enemy->GetAlive() == true)
		{
			enemyMarkers->SetInvisible(false);
			XMFLOAT2 pos = Rader->GetPostion();
			pos.x += 120;
			enemyMarkers->SetPosition({ pos.x + 120.0f * offset.x / m_raderRange, pos.y + 120.0f * offset.z / m_raderRange });
		}
		else
		{
			enemyMarkers->SetInvisible(true);
		}
	}
}

void FreePlayScene::ChangeScene()
{
	m_isEnd = true;
	if (m_enemy->GetAlive() == true)
	{
		m_isEnd = false;
	}
	if (m_player->GetPlayerHP() == 0)
	{
		m_isEnd = true;
	}

	if (m_isEnd == true)
	{
		if (SceneChange::GetInstance()->IsBlockOut(0.1f))
		{
			SceneManager::GetInstance()->ChangeScene("GAME");
		}
	}
}

void FreePlayScene::CheckAllCollisions()
{
#pragma region プレイヤーとエネミーの衝突判定

	CheckPlayer2Enemy();

#pragma endregion

#pragma region プレイヤー弾とエネミーの衝突判定

	CheckPlayerBullets2Enemy();

#pragma endregion
}

void FreePlayScene::CheckPlayer2Enemy()
{
	// 射程内にいるものを探す
	if (m_enemy->GetAlive() == true)
	{
		// 射程内か調べる
		if (m_player->SearchTarget(m_enemy->GetPosition()) && Camera::GetInstance()->ObjectComeInSight(m_enemy->GetPosition()))
		{
			m_lockList->LockOn(m_enemy.get());
		}
		else
		{
			m_lockList->Lost(m_enemy.get());
		}
	}

	// 更新
	m_lockList->Update();

	// 何もロックしてないなら
	if (m_lockList->GetTargetNum() == 0)
	{
		// まっすぐ飛ぶ
		Reticle->SetPosition({ 0, 0 });
		Reticle->SetInvisible(true);

		m_player->SetTarget(false);
	}
	else
	{
		// ロック先に打つ
		Reticle->SetPosition(Camera::GetInstance()->Convert3DPosTo2DPos(m_lockList->GetTargetEnemy()->GetPosition()));
		Reticle->SetInvisible(false);

		m_player->SetTarget(true, m_lockList->GetTargetEnemy()->GetPosition());
	}
}

void FreePlayScene::CheckPlayerBullets2Enemy()
{
	const std::list<Bullet*>& playerBullets = m_player->GetPlayerBullets();
	for (const auto& pB : playerBullets)
	{
		if (pB->GetAlive() == true)
		{
			if (m_enemy->GetAlive() == true && Helper::LengthFloat3(pB->GetPosition(), m_enemy->GetPosition()) < 3.0f)
			{
				pB->SetAlive(false);
				if (m_enemy->EnemyDamage(1) == true)
				{
					m_enemy->SetAlive(false);
					m_lockList->Lost(m_enemy.get());

					int num = Random::GetRanNum(5, 7);
					for (int i = 0; i < num; i++)
					{
						XMFLOAT3 vel = {};
						vel.x += static_cast<float>(Random::GetRanNum(0, 4) - 2) / 10;
						vel.y += static_cast<float>(Random::GetRanNum(0, 2)) / 10;
						vel.z += static_cast<float>(Random::GetRanNum(0, 4) - 2) / 10;
						explosion->Add(60, m_enemy->GetPosition(), vel, { 0, 0, 0 }, { 1.0f, 0.5f, 0.25f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 50.0f, false);
					}
				}
				else
				{
					int num = Random::GetRanNum(3, 4);
					for (int i = 0; i < num; i++)
					{
						XMFLOAT3 pos = m_enemy->GetPosition();
						pos.x += Random::GetRanNum(0, 10) - 5;
						pos.y += Random::GetRanNum(0, 4) - 1;
						pos.z += Random::GetRanNum(0, 10) - 5;
						for (int j = 0; j < 3; j++)
						{
							explosion->Add(60, pos, { 0, 0, 0 }, { 0, 0, 0 }, { 1.0f, 0.5f, 0.25f, 1.0f }, { 0.0f, 0.0f, 0.0f, 0.0f }, 0.0f, 10.0f, false);
						}
					}
				}
			}
		}
	}
}
