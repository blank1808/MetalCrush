#include "EndScene.h"
#include "SceneManager.h"
#include "SceneChange.h"

#include <sstream>
#include <fstream> 
#include <time.h>
#include <stdlib.h>
#include <SafeDelete.h>

using namespace DirectX;
using namespace Microsoft::WRL;

EndScene::~EndScene()
{

}

void EndScene::Initialize()
{
	//スプライト
	endScreen.reset(Sprite::Create(12));
	clearScreen.reset(Sprite::Create(15));

	//オブジェクト

	//パラメーター
	InitializeParameter();

	//オーディオ
	//audio->Initialize();
}

void EndScene::InitializeParameter()
{
	// 変数
	m_isChange = false;
	m_resultScore = 0;
	LoadText();

	// 切り替え
	SceneChange::GetInstance()->SetColor({ 0, 0, 0, 1 });
}

void EndScene::Update()
{
	if (m_isChange == false)
	{
		if (SceneChange::GetInstance()->IsBrighten(0.1f))
		{
			if (Input::GetInstance()->TriggerButton(BUTTON::B))
			{
				m_isChange = true;
			}
		}
	}
	else
	{
		if (SceneChange::GetInstance()->IsBlockOut(0.1f))
		{
			//シーン切り替え
			SceneManager::GetInstance()->ChangeScene("TITLE");
		}
	}
}

void EndScene::Draw()
{
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	//各描画
	DrawObject(cmdList);
}

void EndScene::DrawSprite()
{
	ID3D12GraphicsCommandList* cmdList = DirectXCommon::GetInstance()->GetCmdList();

	//スプライト描画
	Sprite::PreDraw(cmdList);

	if (m_end == "CLEAR")
	{
		clearScreen->Draw();
	} else if (m_end == "GAMEOVER")
	{
		endScreen->Draw();
	}

	SceneChange::GetInstance()->Draw();

	Sprite::PostDraw();
}

void EndScene::DrawObject(ID3D12GraphicsCommandList* cmdList)
{
	
}

void EndScene::LoadText()
{
	std::ifstream file;
	const std::string fileName = "Resources/ScoreList.txt";
	file.open(fileName);
	if (file.fail())
	{
		m_resultScore = 0;
	}

	//1行ずつ読み込む
	std::string line;
	while (getline(file, line))
	{
		//1行分の文字列をストリームに変換して解析しやすくする
		std::istringstream line_stream(line);

		//半角スペースゥ切りで行の先頭文字列を取得
		std::string key;
		getline(line_stream, key, ' ');

		line_stream >> m_end;
		line_stream >> m_resultScore;
	}
	//ファイルを閉じる
	file.close();
}
