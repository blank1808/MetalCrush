#pragma once
#include "Input.h"
#include "Model.h"
#include "Object3d.h"
#include "Sprite.h"
#include "Audio.h"
#include "DebugText.h"
#include "ParticleManager.h"
#include "Collision.h"
#include "Camera.h"
#include "DirectXCommon.h"
#include "BaseScene.h"
#include "LightGroup.h"
#include "FbxLoader.h"
#include "FbxObject3d.h"
#include "Player.h"
#include "Enemy.h"
#include "Bullet.h"
#include "AutoLockOn.h"
#include "CollisionManager.h"
#include "MeshCollider.h"
#include "TouchableObject.h"

#include <Windows.h>
#include <DirectXMath.h>
#include <memory>
#include <array>

/// <summary>
/// ゲームシーン
/// </summary>
class GamePlayScene : public BaseScene
{
public: // エイリアス
	using XMFLOAT2 = DirectX::XMFLOAT2;
	using XMFLOAT3 = DirectX::XMFLOAT3;
	using XMFLOAT4 = DirectX::XMFLOAT4;
	using XMMATRIX = DirectX::XMMATRIX;

private: // サブクラス
	// シェーダーの種類
	enum ShadersType
	{
		ADS, TOON, MONO, BLEND, SPECULAR
	};

	// 壁の配置
	enum WALLNUMBER
	{
		FRONT, BACK, RIGHT, LEFT, UP, DOWN, END
	};

private: // インスタンス
	// ライト
	LightGroup* lightGroup = nullptr;

	// パーティクル
	// 爆発
	std::unique_ptr<ParticleManager> explosion = nullptr;
	// 砂煙
	std::unique_ptr<ParticleManager> sandStorm = nullptr;

	// スプライト
	// レティクル
	std::unique_ptr<Sprite> Reticle = nullptr;
	// HP
	std::unique_ptr<Sprite> HPFrame = nullptr;
	std::unique_ptr<Sprite> HP = nullptr;
	// ブーストゲージ
	std::unique_ptr<Sprite> BoosterFrame = nullptr;
	std::unique_ptr<Sprite> Booster = nullptr;
	// 残弾
	std::unique_ptr<Sprite> NormalBulletNum[2];
	std::unique_ptr<Sprite> HomingBulletNum[2];
	std::unique_ptr<Sprite> BulletNumFrame[2] = {};
	// レーダー
	std::unique_ptr<Sprite> Rader = nullptr;
	std::unique_ptr<Sprite> RaderFrame = nullptr;
	std::vector<std::unique_ptr<Sprite>> enemysMarker;
	// ゲームオーバー
	std::unique_ptr<Sprite> gameOver = nullptr;
	// オーダー
	std::unique_ptr<Sprite> order = nullptr;

	// OBJ
	// 砂漠
	std::unique_ptr<TouchableObject> m_desert = nullptr;
	// 壁
	std::unique_ptr<Object3d> m_skyWall = nullptr;
	// 太陽
	std::unique_ptr<Object3d> m_sun = nullptr;
	// 敵
	std::vector<std::unique_ptr<Enemy>> m_enemy;
	// プレイヤー
	std::unique_ptr<Player> m_player = nullptr;

	// その他
	// ロックオンリスト
	std::unique_ptr<AutoLockOn> m_lockList = nullptr;
	// コリジョンマネージャ
	CollisionManager* m_collisionManager = nullptr;

private: // メンバ変数
	// フレームタイム
	int m_raderAnimation = 0;
	// レーダーの範囲
	float m_raderRange = 0;
	// クリアフラグ
	bool m_isClear = false;
	// クリアフラグ
	bool m_isGameOver = false;
	// リザルトスコア
	int m_resultScore = 0;

public: // メンバ関数
	/// <summary>
	/// デストラクタ
	/// </summary>
	~GamePlayScene() override;

	/// <summary>
	/// 初期化
	/// </summary>
	void Initialize() override;

	/// <summary>
	/// 更新
	/// </summary>
	void Update() override;

	/// <summary>
	/// 描画
	/// </summary>
	void Draw() override;

	/// <summary>
	/// スプライト描画
	/// </summary>
	void DrawSprite() override;

	/// <summary>
	/// 変数初期化
	/// </summary>
	void InitializeParameter();

	/// <summary>
	/// 描画
	/// </summary>
	void DrawObjects(ID3D12GraphicsCommandList* cmdList);

public: // メンバ関数
	/// <summary>
	/// HUD更新
	/// </summary>
	void UpdateHUD();

	/// <summary>
	/// 砂嵐
	/// </summary>
	void SandStormUpdate();

	/// <summary>
	/// ゲームオーバー
	/// </summary>
	void GameOver();

	/// <summary>
	/// スコアの計算
	/// </summary>
	void ComputeScore();

	/// <summary>
	/// テキストに書き出す
	/// </summary>
	void writeText();

	/// <summary>
	/// 衝突判定
	/// </summary>
	void CheckAllCollisions();

	/// <summary>
	/// プレイヤーとエネミーの衝突判定	
	/// </summary>
	void CheckPlayer2Enemy();

	/// <summary>
	/// プレイヤーの弾とエネミーの衝突判定
	/// </summary>
	void CheckPlayerBullets2Enemy();

	/// <summary>
	/// プレイヤーとエネミーの弾の衝突判定
	/// </summary>
	void CheckPlayer2EnemyBullets();
};
