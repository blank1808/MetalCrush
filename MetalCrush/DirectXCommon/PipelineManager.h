#pragma once
#include <Windows.h>
#include <wrl.h>
#include <d3d12.h>
#include <DirectXMath.h>
#include <string>

/// <summary>
/// グラフィックパイプラインマネージャー
/// </summary>
class PipelineManager
{
public: // エイリアス
	// Microsoft::WRL::を省略
	template <class T> using ComPtr = Microsoft::WRL::ComPtr<T>;

private: //メンバ変数
	//パイプラインステート
	ComPtr<ID3D12PipelineState> m_pipelineState;
	//ルートシグネチャ
	ComPtr<ID3D12RootSignature> m_rootSignature;
	// シェーダー名
	std::wstring m_shaderName;

public: //メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	/// <param name="dev">デバイス</param>
	/// <param name="shaderName">シェーダー名</param>
	PipelineManager(ID3D12Device* dev, std::wstring shaderName);

	/// <summary>
	/// シェーダー生成
	/// </summary>
	/// <param name="dev">デバイス</param>
	/// <param name="shaderName">シェーダー名</param>
	void CreateObject3dShaderPipeline(ID3D12Device* dev, std::wstring shaderName);

	/// <summary>
	/// フォンシェーディング
	/// </summary>
	/// <param name="dev">デバイス</param>
	/// <param name="errorBlob">エラーオブジェクト</param>
	/// <param name="gpipeline">グラフィックパイプライン</param>
	void CreatePhongShading(ID3D12Device* dev, ID3DBlob* errorBlob, D3D12_GRAPHICS_PIPELINE_STATE_DESC& gpipeline);

	/// <summary>
	/// テクスチャマッピング
	/// </summary>
	/// <param name="dev">デバイス</param>
	/// <param name="errorBlob">エラーオブジェクト</param>
	/// <param name="gpipeline">グラフィックパイプライン</param>
	void CreateTextureMap(ID3D12Device* dev, ID3DBlob* errorBlob, D3D12_GRAPHICS_PIPELINE_STATE_DESC& gpipeline);

	/// <summary>
	/// パイプラインステートを取得
	/// </summary>
	/// <returns>パイプラインステート</returns>
	inline ID3D12PipelineState *GetPipelineState() { return m_pipelineState.Get(); }

	/// <summary>
	/// ルートシグネチャを取得
	/// </summary>
	/// <returns>ルートシグネチャ</returns>
	inline ID3D12RootSignature *GetRootSignature() { return m_rootSignature.Get(); }

	/// <summary>
	/// シェーダー名取得
	/// </summary>
	/// <returns>シェーダー名</returns>
	inline std::wstring GetShaderName() { return m_shaderName; };
};
