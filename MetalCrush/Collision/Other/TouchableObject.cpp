#include "TouchableObject.h"
#include "MeshCollider.h"
#include "CollisionAttribute.h"
#include <assert.h>

TouchableObject* TouchableObject::Create(const UINT modelNumber, int x, int z)
{
    TouchableObject* instance = new TouchableObject;
    if (instance == nullptr)
    {
        return nullptr;
    }

    if (instance->Initialize(modelNumber, x, z) == false)
    {
        delete instance;
        assert(0);
    }

    return instance;
}

bool TouchableObject::Initialize(const UINT modelNumber, int x, int z)
{
    Object3d::Initialize();

    Model* model = Object3d::GetModel(modelNumber);
    SetModel(model);

    // コライダー追加
    MeshCollider* collider = new MeshCollider(x, z);
    SetCollider(collider);
    collider->ConstructTriangles(model);
    collider->SetAttribute(COLLISION_ATTR_LANDSHAPE);

    return true;
}
