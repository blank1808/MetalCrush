#pragma once
#include "CollisionPrimitive.h"
#include "RayCast.h"
#include "QueryCallBack.h"

#include <d3d12.h>
#include <memory>
#include <forward_list>

class BaseCollider;

/// <summary>
/// 衝突マネージャ
/// </summary>
class CollisionManager
{
public: // 静的メンバ関数
	static CollisionManager* GetInstance();

private: // メンバ変数
	std::forward_list<BaseCollider*> m_colliders;

private: // メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	CollisionManager() = default;

	/// <summary>
	/// デストラクタ
	/// </summary>
	~CollisionManager() = default;

	/// <summary>
	/// コピーインストラクタ
	/// </summary>
	/// <param name="">インストラクタ</param>
	CollisionManager(const CollisionManager&) = delete;

	/// <summary>
	/// コピー代入演算子
	/// </summary>
	/// <param name="">インストラクタ</param>
	/// <returns>衝突マネージャ</returns>
	CollisionManager& operator=(const CollisionManager&) = delete;

public: // メンバ関数
	/// <summary>
	/// コライダー追加
	/// </summary>
	/// <param name="collider">コライダー</param>
	inline void AddCollider(BaseCollider* collider)
	{
		m_colliders.emplace_front(collider);
	}

	/// <summary>
	/// コライダー削除
	/// </summary>
	/// <param name="collider">コライダー</param>
	inline void RemoveCollider(BaseCollider* collider)
	{
		m_colliders.remove(collider);
	}

	/// <summary>
	/// レイキャスト
	/// </summary>
	/// <param name="ray">レイ</param>
	/// <param name="attribute">当たり判定属性</param>
	/// <param name="hitInfo">衝突情報</param>
	/// <param name="maxDistance">最大距離</param>
	/// <returns>成否</returns>
	bool Raycast(const Ray& ray, unsigned short attribute, RayCastHit* hitInfo = nullptr, float maxDistance = D3D12_FLOAT32_MAX);

	/// <summary>
	/// レイキャスト(属性なし)
	/// </summary>
	/// <param name="ray">レイ</param>
	/// <param name="attribute">当たり判定属性</param>
	/// <param name="hitInfo">衝突情報</param>
	/// <param name="maxDistance">最大距離</param>
	/// <returns>成否</returns>
	bool RaycastNoAtt(const Ray& ray, RayCastHit* hitInfo = nullptr, float maxDistance = D3D12_FLOAT32_MAX);

	/// <summary>
	/// 球による衝突全検索
	/// </summary>
	/// <param name="sphere">球</param>
	/// <param name="callBack">衝突時コールバック</param>
	/// <param name="attribute">対象の属性</param>
	void QuerySphere(const Sphere& sphere, QueryCallBack* callBack, unsigned short attribute = (unsigned short)0xffffffff);

	/// <summary>
	/// すべての衝突チェック
	/// </summary>
	void CheckAllCollisions();
};
