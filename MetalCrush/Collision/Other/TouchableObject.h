#pragma once
#include "Object3d.h"

/// <summary>
/// 接触可能オブジェクト
/// </summary>
class TouchableObject : public Object3d
{
public: // メンバ関数
	/// <summary>
	/// オブジェクト生成
	/// </summary>
	/// <param name="model">モデルデータ</param>
	/// <param name="x">横を何分割にするか</param>
	/// <param name="z">縦を何分割にするか</param>
	/// <returns>接触可能オブジェクト</returns>
	static TouchableObject* Create(const UINT modelNumber, int x = 1, int z = 1);

	/// <summary>
	/// 初期化
	/// </summary>
	/// <param name="model">モデルデータ</param>
	/// <param name="x">横を何分割にするか</param>
	/// <param name="z">縦を何分割にするか</param>
	/// <returns>成否</returns>
	bool Initialize(const UINT modelNumber, int x, int z);
};
