#pragma once

enum CollisionShapeType
{
	SHAPE_UNKNOWN = -1, //未設定

	COLLISONSHAPE_SPHERE, //球
	COLLISONSHAPE_BOX, // ボックス
	COLLISONSHAPE_MESH, //メッシュ
};