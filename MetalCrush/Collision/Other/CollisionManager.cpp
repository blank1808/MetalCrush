#include "CollisionManager.h"
#include "Collision.h"
#include "MeshCollider.h"

using namespace DirectX;

CollisionManager* CollisionManager::GetInstance()
{
	static CollisionManager instance;
	return &instance;
}

bool CollisionManager::Raycast(const Ray& ray, unsigned short attribute, RayCastHit* hitInfo, float maxDistance)
{
	bool result = false;
	// 走査用
	std::forward_list<BaseCollider*>::iterator it;
	// 最短のコライダー
	std::forward_list<BaseCollider*>::iterator it_hit;
	// 距離
	float distance = maxDistance;
	// 交点
	XMVECTOR inter;

	it = m_colliders.begin();
	for (; it != m_colliders.end(); ++it)
	{
		BaseCollider* colA = *it;

		if (!(colA->m_attribute & attribute))
		{
			continue;
		}

		// 球
		if (colA->GetShapeType() == COLLISONSHAPE_SPHERE)
		{
			Sphere* sphere = dynamic_cast<Sphere*>(colA);
			float tempDistance;
			XMVECTOR tempInter;
			// 当たってないなら除外
			if (!Collision::CheckRay2Sphere(ray, *sphere, &tempDistance, &tempInter))
			{
				continue;
			}
			// 距離が最短でないなら除外
			if (tempDistance >= distance)
			{
				continue;
			}
			result = true;
			distance = tempDistance;
			inter = tempInter;
			it_hit = it;
		}
		// 面
		else if (colA->GetShapeType() == COLLISONSHAPE_MESH)
		{
			MeshCollider* meshCollider = dynamic_cast<MeshCollider*>(colA);

			float tempDistance;
			DirectX::XMVECTOR tempInter;
			// 当たってないなら除外
			if (!meshCollider->CheckCollisionRay(ray, &tempDistance, &tempInter))
			{
				continue;
			}
			// 距離が最短でないなら除外
			if (distance <= tempDistance)
			{
				continue;
			}

			result = true;
			distance = tempDistance;
			inter = tempInter;
			it_hit = it;
		}
	}

	// 結果の書き込み
	if (result && hitInfo)
	{
		hitInfo->distance = distance;
		hitInfo->inter = inter;
		hitInfo->collider = *it_hit;
		hitInfo->object = hitInfo->collider->GetObject3d();
	}

	return result;
}

bool CollisionManager::RaycastNoAtt(const Ray& ray, RayCastHit* hitInfo, float maxDistance)
{
	return Raycast(ray, 0xffff, hitInfo, maxDistance);
}

void CollisionManager::QuerySphere(const Sphere& sphere, QueryCallBack* callBack, unsigned short attribute)
{
	// 走査用
	std::forward_list<BaseCollider*>::iterator it;

	it = m_colliders.begin();
	for (; it != m_colliders.end(); ++it)
	{
		BaseCollider* col = *it;

		if (!(col->m_attribute & attribute))
		{
			continue;
		}

		// 球
		if (col->GetShapeType() == COLLISONSHAPE_SPHERE)
		{
			Sphere* sphereB = dynamic_cast<Sphere*>(col);
			XMVECTOR tempInter;
			XMVECTOR tempReject;
			// 当たってないなら除外
			if (!Collision::CheckSphere2Sphere(sphere, *sphereB, &tempInter, &tempReject))
			{
				continue;
			}

			// 交差情報をセット
			QueryHit info;
			info.collider = col;
			info.object = col->GetObject3d();
			info.inter = tempInter;
			info.reject = tempReject;

			// クエリーコールバック
			if (!callBack->OnQueryHit(info))
			{
				return;
			}
		}
		// 面
		else if (col->GetShapeType() == COLLISONSHAPE_MESH)
		{
			MeshCollider* meshCollider = dynamic_cast<MeshCollider*>(col);
			XMVECTOR tempInter;
			XMVECTOR tempReject;
			// 当たってないなら除外
			if (!meshCollider->CheckCollisionSphere(sphere, &tempInter, &tempReject))
			{
				continue;
			}

			// 交差情報をセット
			QueryHit info;
			info.collider = col;
			info.object = col->GetObject3d();
			info.inter = tempInter;
			info.reject = tempReject;

			// クエリーコールバック
			if (!callBack->OnQueryHit(info))
			{
				return;
			}
		}
	}
}

void CollisionManager::CheckAllCollisions()
{
	std::forward_list<BaseCollider*>::iterator itA;
	std::forward_list<BaseCollider*>::iterator itB;

	// すべての組み合わせ
	itA = m_colliders.begin();
	for (; itA != m_colliders.end(); ++itA)
	{
		itB = itA;
		++itB;
		for (; itB != m_colliders.end(); ++itB)
		{
			BaseCollider* colA = *itA;
			BaseCollider* colB = *itB;

			// 球と球
			if (colA->GetShapeType() == COLLISONSHAPE_SPHERE && colB->GetShapeType() == COLLISONSHAPE_SPHERE)
			{
				Sphere* sphereA = dynamic_cast<Sphere*>(colA);
				Sphere* sphereB = dynamic_cast<Sphere*>(colB);
				DirectX::XMVECTOR inter;

				// 球と球の判定
				if (Collision::CheckSphere2Sphere(*sphereA, *sphereB, &inter) == true)
				{
					colA->OnCollision(CollisionInfo(colB->GetObject3d(), colB, inter));
					colB->OnCollision(CollisionInfo(colA->GetObject3d(), colA, inter));
				}
			}
			// 面と球
			else if (colA->GetShapeType() == COLLISONSHAPE_MESH && colB->GetShapeType() == COLLISONSHAPE_SPHERE)
			{
				MeshCollider* meshCollider = dynamic_cast<MeshCollider*>(colA);
				Sphere* sphere = dynamic_cast<Sphere*>(colB);
				DirectX::XMVECTOR inter;

				// 球と球の判定
				if (meshCollider->CheckCollisionSphere(*sphere, &inter) == true)
				{
					colA->OnCollision(CollisionInfo(colB->GetObject3d(), colB, inter));
					colB->OnCollision(CollisionInfo(colA->GetObject3d(), colA, inter));
				}
			}
			// 球と面
			else if (colA->GetShapeType() == COLLISONSHAPE_SPHERE && colB->GetShapeType() == COLLISONSHAPE_MESH)
			{
				Sphere* sphere = dynamic_cast<Sphere*>(colA);
				MeshCollider* meshCollider = dynamic_cast<MeshCollider*>(colB);
				DirectX::XMVECTOR inter;

				// 球と球の判定
				if (meshCollider->CheckCollisionSphere(*sphere, &inter) == true)
				{
					colA->OnCollision(CollisionInfo(colB->GetObject3d(), colB, inter));
					colB->OnCollision(CollisionInfo(colA->GetObject3d(), colA, inter));
				}
			}
		}
	}
}
