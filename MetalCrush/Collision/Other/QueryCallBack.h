#pragma once
#include <DirectXMath.h>

class Object3d;
class BaseCollider;

/// <summary>
/// クエリーの情報
/// </summary>
struct QueryHit
{
	// 衝突相手のオブジェクト
	Object3d* object = nullptr;
	// 衝突相手のコライダー
	BaseCollider* collider = nullptr;
	// 衝突点
	DirectX::XMVECTOR inter;
	// 排斥ベクトル
	DirectX::XMVECTOR reject;
};

/// <summary>
/// クエリーで交差を検知したときの動作を規定する
/// </summary>
class QueryCallBack
{
public: // メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	QueryCallBack() = default;

	/// <summary>
	/// デストラクタ
	/// </summary>
	~QueryCallBack() = default;

	/// <summary>
	/// 交差時コールバック
	/// </summary>
	/// <param name="info">交差情報</param>
	/// <returns>クエリーを続けるならture</returns>
	virtual bool OnQueryHit(const QueryHit& info) = 0;
};
