#pragma once
#include "BaseCollider.h"
#include "CollisionPrimitive.h"

#include <DirectXMath.h>

/// <summary>
/// メッシュ衝突判定
/// </summary>
class MeshCollider : public BaseCollider
{
private: // サブクラス
	// エリアの番号
	struct AreaNumber
	{
		// 横のエリア
		int x = 0;
		// 縦のエリア
		int z = 0;

		// コンストラクタ
		AreaNumber(int areaX, int areaZ)
		{
			x = areaX;
			z = areaZ;
		}
	};

	// 三角形の情報
	struct AreaInfo
	{
		// 三角形の情報
		std::vector<Triangle> triangle;

		// デストラクタ
		~AreaInfo()
		{
			triangle.clear();
		}
	};

private: // メンバ変数
	// 横の最大エリア数
	int m_maxAreaX = 0;
	// 縦の最大エリア数
	int m_maxAreaZ = 0;
	// 三角形の配列
	std::vector<std::vector<AreaInfo*>> m_area;
	// ワールド行列の逆行列
	DirectX::XMMATRIX m_invMatWorld = {};
	// 正の最大座標
	DirectX::XMFLOAT3 positivePosition = {};
	// 負の最大座標
	DirectX::XMFLOAT3 negativePosition = {};

public: // メンバ関数
	/// <summary>
	/// コンストラクタ
	/// </summary>
	MeshCollider(int x = 1, int z = 1);
	
	/// <summary>
	/// デストラクタ
	/// </summary>
	~MeshCollider();

	/// <summary>
	/// 三角形の配列を作る
	/// </summary>
	/// <param name="model">モデルデータ</param>
	/// <param name="div">縦横に何分割するか</param>
	void ConstructTriangles(Model* model);

	/// <summary>
	/// 更新
	/// </summary>
	void Update() override;

	/// <summary>
	/// 三角形をエリア分け
	/// </summary>
	/// <param name="tri">三角形の情報</param>
	/// <returns>エリア番号</returns>
	void SortOutTriangleArea(const Triangle& tri);

	/// <summary>
	/// 座標がどのエリアにいるか
	/// </summary>
	/// <param name="pos">座標</param>
	/// <returns>該当のエリア</returns>
	std::vector<Triangle> GetPositionArea(const DirectX::XMVECTOR& pos);

	/// <summary>
	/// 座標がどのエリアにいるか
	/// </summary>
	/// <param name="pos">座標</param>
	/// <returns>該当のエリアの番号</returns>
	AreaNumber GetPositionAreaNum(const DirectX::XMVECTOR& pos);

	/// <summary>
	/// 球との当たり判定
	/// </summary>
	/// <param name="sphere">球</param>
	/// <param name="inter">交点</param>
	/// <returns>成否</returns>
	bool CheckCollisionSphere(const Sphere& sphere, DirectX::XMVECTOR *inter = nullptr, DirectX::XMVECTOR* reject = nullptr);

	/// <summary>
	/// レイとの当たり判定
	/// </summary>
	/// <param name="ray">レイ</param>
	/// <param name="distance">距離</param>
	/// <param name="inter">交点</param>
	/// <returns>成否</returns>
	bool CheckCollisionRay(const Ray& ray, float* distance = nullptr, DirectX::XMVECTOR *inter = nullptr);
};
