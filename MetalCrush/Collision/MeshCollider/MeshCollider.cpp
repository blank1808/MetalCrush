#include "MeshCollider.h"
#include "Collision.h"
#include <assert.h>

using namespace DirectX;

MeshCollider::MeshCollider(int x, int z)
{
	// 最大エリア数セット
	m_maxAreaX = x;
	m_maxAreaZ = z;

	// 配列作成
	for (int i = 0; i < m_maxAreaZ; i++)
	{
		m_area.emplace_back();
		for (int j = 0; j < m_maxAreaX; j++)
		{
			if (m_area.size() <= i)
			{
				assert(0);
			}

			m_area[i].emplace_back(new AreaInfo);
		}
	}

	// 形状セット
	m_shapeType = COLLISONSHAPE_MESH;
}

MeshCollider::~MeshCollider()
{
	for (auto& m : m_area)
	{
		m.clear();
	}
	m_area.clear();
}

void MeshCollider::ConstructTriangles(Model* model)
{
	// 正と負の最大値を取得
	positivePosition = model->GetPositivePos();
	negativePosition = model->GetNegativePos();

	// メッシュ配列取得
	const std::vector<std::unique_ptr<Mesh>>& meshes = model->GetMeshes();
	// 三角形の開始番号
	uint32_t start = 0;
	std::vector<std::unique_ptr<Mesh>>::const_iterator it = meshes.cbegin();
	for (; it != meshes.cend(); ++it)
	{
		Mesh* mesh = it->get();
		const std::vector<Mesh::VertexPosNormalUv>& vertices = mesh->GetVertices();
		const std::vector<uint32_t>& indices = mesh->GetIndices();

		// 三角形の数
		uint32_t triangleNum = static_cast<uint32_t>(indices.size()) / 3;
		for (uint32_t i = 0; i < triangleNum; i++)
		{
			// 三角形を作成
			Triangle tri;
			uint32_t idx0 = indices[i * 3 + 0];
			uint32_t idx1 = indices[i * 3 + 1];
			uint32_t idx2 = indices[i * 3 + 2];

			tri.p0 = { vertices[idx0].pos.x, vertices[idx0].pos.y, vertices[idx0].pos.z, 1 };
			tri.p1 = { vertices[idx1].pos.x, vertices[idx1].pos.y, vertices[idx1].pos.z, 1 };
			tri.p2 = { vertices[idx2].pos.x, vertices[idx2].pos.y, vertices[idx2].pos.z, 1 };
			tri.ComputeNormal();

			// 該当エリアに格納
			SortOutTriangleArea(tri);
		}
	}
}

void MeshCollider::Update()
{
	m_invMatWorld = XMMatrixInverse(nullptr, GetObject3d()->GetMatWorld());
}

void MeshCollider::SortOutTriangleArea(const Triangle& tri)
{
	// 横の長さ
	float width = positivePosition.x - negativePosition.x;
	// 縦の長さ
	float height = positivePosition.z - negativePosition.z;
	// 横のエリア一つ分の長さ
	float wDiv = width / m_maxAreaX;
	// 縦のエリア一つ分の長さ
	float hDiv = height / m_maxAreaZ;

	// p0
	// 横のエリアの番号
	int wNumP0 = static_cast<int>((tri.p0.m128_f32[0] - negativePosition.x) / wDiv);
	if (m_maxAreaX <= wNumP0)
	{
		wNumP0 = m_maxAreaX - 1;
	}
	// 縦のエリアの番号
	int hNumP0 = static_cast<int>((tri.p0.m128_f32[2] - negativePosition.z) / hDiv);
	if (m_maxAreaZ <= hNumP0)
	{
		hNumP0 = m_maxAreaZ - 1;
	}
	// 該当エリアに入れる
	m_area[hNumP0][wNumP0]->triangle.emplace_back(tri);

	// p1
	// 横のエリアの番号
	int wNumP1 = static_cast<int>((tri.p1.m128_f32[0] - negativePosition.x) / wDiv);
	if (m_maxAreaX <= wNumP1)
	{
		wNumP1 = m_maxAreaX - 1;
	}
	// 縦のエリアの番号
	int hNumP1 = static_cast<int>((tri.p1.m128_f32[2] - negativePosition.z) / hDiv);
	if (m_maxAreaZ <= hNumP1)
	{
		hNumP1 = m_maxAreaZ - 1;
	}
	// p0と違うなら
	if (wNumP0 != wNumP1 || hNumP0 != hNumP1)
	{
		m_area[hNumP1][wNumP1]->triangle.emplace_back(tri);
	}

	// p2
	// 横のエリアの番号
	int wNumP2 = static_cast<int>((tri.p2.m128_f32[0] - negativePosition.x) / wDiv);
	if (m_maxAreaX <= wNumP2)
	{
		wNumP2 = m_maxAreaX - 1;
	}
	// 縦のエリアの番号
	int hNumP2 = static_cast<int>((tri.p2.m128_f32[2] - negativePosition.z) / hDiv);
	if (m_maxAreaZ <= hNumP2)
	{
		hNumP2 = m_maxAreaZ - 1;
	}
	// p0と違うなら
	if (wNumP0 != wNumP2 || hNumP0 != hNumP2)
	{
		if (wNumP1 != wNumP2 || hNumP1 != hNumP2)
		{
			// 該当エリアに入れる
			m_area[hNumP2][wNumP2]->triangle.emplace_back(tri);
		}
	}
}

std::vector<Triangle>  MeshCollider::GetPositionArea(const DirectX::XMVECTOR& pos)
{
	// 横の長さ
	float width = positivePosition.x - negativePosition.x;
	// 縦の長さ
	float height = positivePosition.z - negativePosition.z;
	// 横のエリア一つ分の長さ
	float wDiv = width / m_maxAreaX;
	// 縦のエリア一つ分の長さ
	float hDiv = height / m_maxAreaZ;

	// 横のエリアの番号
	int wNum = static_cast<int>((pos.m128_f32[0] - negativePosition.x) / wDiv);
	if (m_maxAreaX <= wNum)
	{
		wNum = m_maxAreaX - 1;
	}
	// 縦のエリアの番号
	int hNum = static_cast<int>((pos.m128_f32[2] - negativePosition.z) / hDiv);
	if (m_maxAreaZ <= hNum)
	{
		hNum = m_maxAreaZ - 1;
	}

	return m_area[hNum][wNum]->triangle;
}

MeshCollider::AreaNumber MeshCollider::GetPositionAreaNum(const DirectX::XMVECTOR& pos)
{
	// 横の長さ
	float width = positivePosition.x - negativePosition.x;
	// 縦の長さ
	float height = positivePosition.z - negativePosition.z;
	// 横のエリア一つ分の長さ
	float wDiv = width / m_maxAreaX;
	// 縦のエリア一つ分の長さ
	float hDiv = height / m_maxAreaZ;

	// 横のエリアの番号
	int wNum = static_cast<int>((pos.m128_f32[0] - negativePosition.x) / wDiv);
	if (m_maxAreaX <= wNum)
	{
		wNum = m_maxAreaX - 1;
	}
	// 縦のエリアの番号
	int hNum = static_cast<int>((pos.m128_f32[2] - negativePosition.z) / hDiv);
	if (m_maxAreaZ <= hNum)
	{
		hNum = m_maxAreaZ - 1;
	}

	return AreaNumber(wNum, hNum);
}

bool MeshCollider::CheckCollisionSphere(const Sphere& sphere, DirectX::XMVECTOR* inter, DirectX::XMVECTOR* reject)
{
	// 球
	Sphere local;
	local.center = XMVector3Transform(sphere.center, m_invMatWorld);
	local.radius *= XMVector3Length(m_invMatWorld.r[0]).m128_f32[0];

	// 三角形
	std::vector<Triangle> triangles = GetPositionArea(local.center);
	std::vector<Triangle>::const_iterator it = triangles.cbegin();
	for (; it != triangles.cend(); ++it)
	{
		const Triangle& triangle = *it;
		if (Collision::CheckSphere2Triangle(local, triangle, inter, reject) == true)
		{
			if (inter)
			{
				const XMMATRIX& matWorld = GetObject3d()->GetMatWorld();
				// ワールド座標の交点
				*inter = XMVector3Transform(*inter, matWorld);
			}
			if (reject)
			{
				const XMMATRIX& matWorld = GetObject3d()->GetMatWorld();
				// 排斥ベクトルに変換
				*reject = XMVector3TransformNormal(*reject, matWorld);
			}

			return true;
		}
	}

	return false;
}

bool MeshCollider::CheckCollisionRay(const Ray& ray, float* distance, DirectX::XMVECTOR* inter)
{
	// レイ
	Ray local;
	local.start = XMVector3Transform(ray.start, m_invMatWorld);
	local.dir = XMVector3TransformNormal(ray.dir, m_invMatWorld);

	// 三角形の場所
	AreaNumber triangleAreA = GetPositionAreaNum(local.start);
	AreaNumber triangleAreB = GetPositionAreaNum(local.start + local.dir);
	if (triangleAreB.x < triangleAreA.x)
	{
		int tmp = triangleAreA.x;
		triangleAreA.x = triangleAreB.x;
		triangleAreB.x = tmp;
	}
	if (triangleAreB.z < triangleAreA.z)
	{
		int tmp = triangleAreA.z;
		triangleAreA.z = triangleAreB.z;
		triangleAreB.z = tmp;
	}

	// 三角形
	for (int x = triangleAreA.x; x <= triangleAreB.x; x++)
	{
		for (int z = triangleAreA.z; z <= triangleAreB.z; z++)
		{
			std::vector<Triangle> triangles = m_area[z][x]->triangle;
			std::vector<Triangle>::const_iterator it = triangles.cbegin();
			for (; it != triangles.cend(); ++it)
			{
				const Triangle& triangle = *it;
				XMVECTOR tempInter;
				if (Collision::CheckRay2Triangle(local, triangle, nullptr, &tempInter) == true)
				{
					const XMMATRIX& matWorld = GetObject3d()->GetMatWorld();
					// ワールド座標の交点
					tempInter = XMVector3Transform(tempInter, matWorld);

					if (distance)
					{
						XMVECTOR sub = tempInter - ray.start;
						*distance = XMVector3Dot(sub, ray.dir).m128_f32[0];
					}
					if (inter)
					{
						*inter = tempInter;
					}

					return true;
				}
			}
		}
	}

	return false;
}
